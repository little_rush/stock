/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.ScriptResult;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.google.gson.Gson;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author little_rush
 */
public class execks {

    Date date = new Date();
    Calendar CD = Calendar.getInstance();
    private String time = null;

    public List exec(ServletContext context, String stock) throws IOException {
//        URL url=new URL("http://lendle.imsofa.rocks:8080/GoodinfoStockCrawlerService/info?stock="+stock);
//        InputStream input=url.openStream();
//        try{
//            String json=IOUtils.toString(input, "utf-8");
//            Logger.getLogger(this.getClass().getName()).info(json.substring(0, 1000)+"......");
//            List ks=new Gson().fromJson(json, List.class);
//            return ks;
//        }finally{
//            if(input!=null){
//                input.close();
//            }
//        }
        final WebClient webClient = new WebClient(BrowserVersion.INTERNET_EXPLORER_11);
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.getOptions().setActiveXNative(false);
        webClient.getOptions().setAppletEnabled(false);
        final HtmlPage page = webClient.getPage("http://www.goodinfo.tw/stockinfo/ShowK_Chart.asp?CHT_NM=%E5%80%8B%E8%82%A1K%E7%B7%9A%E5%9C%96&STOCK_ID=" + stock + "&CHT_CAT=%E6%97%A5%E7%B7%9A%E5%9C%96&CHT_CAT2=DATE&PERIOD=365");
        page.executeJavaScript("var s=document.createElement(\"script\");  s.setAttribute(\"type\", \"text/javascript\");s.setAttribute(\"src\", \"http://code.jquery.com/jquery-1.11.0.min.js\");document.body.appendChild(s);");
        DomNodeList<DomElement> domNodeList = page.getElementsByTagName("script");
        String chartDataURL = null;
        for (int i = 0; i < domNodeList.getLength(); i++) {
            DomElement scriptElement = domNodeList.get(i);
            String src = scriptElement.getAttribute("src");
            //Logger.getLogger(this.getClass().getName()).info("src=" + src);
            if (src.contains("ShowK_ChartData.asp")) {
                chartDataURL = src;
                break;
            }
        }
        //System.out.println("chartDataURL" + chartDataURL);
        //page.executeJavaScript("var s=document.createElement(\"script\");  s.setAttribute(\"type\", \"text/javascript\");s.setAttribute(\"src\", \" " + chartDataURL + " \");document.body.appendChild(s);");
        Reader ksReader = new InputStreamReader(new FileInputStream(context.getRealPath("/WEB-INF/ks.js")), "utf-8");
        ScriptResult result = page.executeJavaScript(IOUtils.toString(ksReader).replace("$STOCK_ID", stock));
        ksReader.close();
        String resultString = (String) result.getJavaScriptResult();
        Logger.getLogger(this.getClass().getName()).info("resultString="+resultString);
        Gson gson = new Gson();
        Map resultMap = gson.fromJson(resultString, Map.class);
        Map Records = (Map) resultMap.get("Records");
        List ks = new ArrayList();
        Logger.getLogger(this.getClass().getName()).info("size="+((List) Records.get("RPT_TIME")).size());
            for (int i = 0; i < ((List) Records.get("RPT_TIME")).size(); i++) {
                HashMap mapRecords = new HashMap();
                mapRecords.put("MACD", ((List) Records.get("MACD")).get(i));
                mapRecords.put("K值", ((List) Records.get("K值")).get(i));
                mapRecords.put("D值", ((List) Records.get("D值")).get(i));
                mapRecords.put("RSV", ((List) Records.get("RSV")).get(i));
                mapRecords.put("OSC", ((List) Records.get("OSC")).get(i));
                mapRecords.put("DIF", ((List) Records.get("DIF")).get(i));
                mapRecords.put("成交量", ((List) Records.get("成交量")).get(i));
                mapRecords.put("收盤價", ((List) Records.get("收盤價")).get(i));
                mapRecords.put("開盤價", ((List) Records.get("開盤價")).get(i));
                mapRecords.put("RPT_TIME", (((List) Records.get("RPT_TIME")).get(i)));
                ks.add(mapRecords);
            }
        ksReader.close();
        return ks;

    }
}
