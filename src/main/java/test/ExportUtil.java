/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 *將數據寫入CSV檔
 * @author treelazy
 */
public class ExportUtil {

    public static void toCSV(String stock) throws SQLException, IOException{
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "root", "stock");
        //Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "stock", "stock");
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery("select * from stock where stock_id ='" + stock + "'");
        Calendar CD = Calendar.getInstance();
        int YY = CD.get(Calendar.YEAR);
        int MM = CD.get(Calendar.MONTH) + 1;
        int DD = CD.get(Calendar.DATE);
        int HH = CD.get(Calendar.HOUR);
        int NN = CD.get(Calendar.MINUTE);
        String year = Integer.toString(YY);
        String month = Integer.toString(MM);
        String day = Integer.toString(DD);
        String time = year + month + day + stock;
        //PrintWriter writer = new PrintWriter(new FileWriter("/home/little_rush/stock/" + time + ".csv"));//ilab
       PrintWriter writer = new PrintWriter(new FileWriter("C:\\Users\\treelazy\\Documents\\NetBeansProjects\\stock\\" + time + ".csv"));//littlerush
        writer.write("day" + "," + "close" + "," + "open" + "," + "volume" + "," + "DIF" + "," + "OSC" + "," + "RSV" + "," + "D" + "," + "K" + "," + "MACD" + "\n");
        while (rs.next()) {
            HashMap mapRecords = new HashMap();
            mapRecords.put("MACD", rs.getDouble("MACD")); //前面放Key值,後面放資料庫的值
            mapRecords.put("K值", rs.getDouble("K"));
            mapRecords.put("D值", rs.getDouble("D"));
            mapRecords.put("RSV", rs.getDouble("RSV"));
            mapRecords.put("OSC", rs.getDouble("OSC"));
            mapRecords.put("DIF", rs.getDouble("DIF"));
            mapRecords.put("成交量", rs.getDouble("volume"));
            mapRecords.put("收盤價", rs.getDouble("close"));
            mapRecords.put("RPT_TIME", rs.getString("day"));
            writer.write(rs.getString("day") + "," + rs.getDouble("close") + "," + rs.getDouble("open") + "," + rs.getDouble("volume") + "," + rs.getDouble("DIF") + "," + rs.getDouble("OSC") + "," + rs.getDouble("RSV") + "," + rs.getDouble("D") + "," + rs.getDouble("K") + "," + rs.getDouble("MACD") + "\n");
        }
        writer.close();
    }
}
