/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 *
 * @author treelazy
 */
public class useremail extends Authenticator{

    private String USER;
    private String PASSWORD;

    //空的建構子
    public useremail() {

        super();
    }

    //可以讓外部傳送帳號密碼進來的建構子
    public useremail(String user, String password) {
        this();
        this.USER = user;
        this.PASSWORD = password;
    }

    protected PasswordAuthentication getPasswordAuthentication() {
        String username = USER;
        String password = PASSWORD;
        if ((username != null) && (username.length() > 0)
                && (password != null)
                && (password.length() > 0)) {

            return new PasswordAuthentication(username, password);
        }

        return null;
    }

}
