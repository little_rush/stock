/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import ognl.OgnlException;

/**
 * 預測數據分析
 *
 * @author treelazy
 */
public class AnalyzingDaemon extends Thread {

    private ArrayList<PredictLastDataResult> predictLastDataresult = new ArrayList<PredictLastDataResult>();
    private RimplementQueue queue = null;
    private ServletContext servletContext = null;
    private StockWebDataCrawlerJobQueue stockWebDataCrawlerJobQueue = null;
    private SendemailQeeue sendemailQeeue = null;

    private List<AnalyzingPlugin> plugins = new ArrayList<AnalyzingPlugin>();

    AnalyzingDaemon(ServletContext servletContext, RimplementQueue rimplementQueue) {
        this.servletContext = servletContext;
        queue = rimplementQueue;
        this.stockWebDataCrawlerJobQueue = (StockWebDataCrawlerJobQueue) servletContext.getAttribute("StockWebDataCrawlerJobQueue");
        this.sendemailQeeue = (SendemailQeeue) servletContext.getAttribute("SendemailQeeue");
        this.plugins.add(new Rimplement());
    }

    Calendar CD = Calendar.getInstance();
    private boolean running = true;

    public void run() {
        while (running) {
            String stock = queue.getStock();
            if (stock != null) {
                try {
                    Logger.getLogger(this.getClass().getName()).info("analyze for " + stock);
                    int YY = CD.get(Calendar.YEAR);
                    int MM = CD.get(Calendar.MONTH) + 1;
                    int DD = CD.get(Calendar.DATE);
                    String year = Integer.toString(YY);
                    String month = Integer.toString(MM);
                    String day = Integer.toString(DD);
                    String m = String.format("%02d", MM);
                    String d = String.format("%02d", DD);
                    String time = year + month + day + stock;
                    String now = year + "-" + m + "-" + d;
                    Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "root", "stock");
                    //Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "stock", "stock");
                    Statement stmt = connection.createStatement();
                    ResultSet rs = stmt.executeQuery("select * from predictlastdata where today='" + now + "' and stock_id='" + stock + "'");
                    boolean exists = false;
                    if (rs.next()) {
                        exists = true;
                    } else {
                        exists = false;
                    }
                    if (!exists) {
                        //File file = new File("/home/little_rush/stock/" + time + ".csv");
                        File file = new File("C:/Users/treelazy/Documents/NetBeansProjects/stock/" + time + ".csv");
                        if (file.exists()) {
                            try {
                                for (AnalyzingPlugin plugin : plugins) {
                                    plugin.analyze(servletContext, stock, file);
                                }
                            } catch (Exception ex) {
                                Logger.getLogger(AnalyzingDaemon.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            sendemailQeeue.addStock(stock);
                            Logger.getLogger(this.getClass().getName()).info("finish analyze for " + stock);
                        } else {
                            stockWebDataCrawlerJobQueue.addStock(stock);
                            Logger.getLogger(this.getClass().getName()).info("again write for " + stock);
                        }
                    } else {
                        sendemailQeeue.addStock(stock);
                        Logger.getLogger(this.getClass().getName()).info("anaylsis for sent email" + stock);
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(AnalyzingDaemon.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void shutdown() {
        this.running = false;
        this.interrupt();
    }

}
