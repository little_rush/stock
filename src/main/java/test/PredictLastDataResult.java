/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

/**
 *
 * @author treelazy
 */
public class PredictLastDataResult {
    private double nextClose;
    private double close;
    private double repeatCount;
    private String today;

    public String getToday() {
        return today;
    }

    public void setToday(String today) {
        this.today = today;
    }
    public double getNextClose() {
        return nextClose;
    }

    public void setNextClose(double nextClose) {
        this.nextClose = nextClose;
    }

    public double getClose() {
        return close;
    }

    public void setClose(double close) {
        this.close = close;
    }

    public double getRepeatCount() {
        return repeatCount;
    }

    public void setRepeatCount(double repeatCount) {
        this.repeatCount = repeatCount;
    }
    
}
