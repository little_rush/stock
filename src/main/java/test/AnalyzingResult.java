/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.Date;

/**
 *SVM分析結果
 * @author little_rush
 */
public class AnalyzingResult {
    private String date;
    private boolean rise;
    private double value;
    private double confidence;
    private double [] estimate=null;

    public double[] getEstimate() {
        return estimate;
    }

    public void setEstimate(double[] estimate) {
        this.estimate = estimate;
    }
    
    

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean getRise() {
        return rise;
    }

    public void setRise(boolean rise) {
        this.rise = rise;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getConfidence() {
        return confidence;
    }

    public void setConfidence(double confidence) {
        this.confidence = confidence;
    }
    void setSvm(double[] temp) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
