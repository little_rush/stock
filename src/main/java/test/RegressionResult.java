/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

/**
 *回歸分析結果
 * @author little_rush
 */
public class RegressionResult extends AnalyzingResult{

    private double rSquared;
    private double[] parameters;
    private double[] result;
    private String [] fields=null;

    public String[] getFields() {
        return fields;
    }

    public void setFields(String[] fields) {
        this.fields = fields;
    }

    public double getSquared() {
        return rSquared;
    }

    public void setSquared(double rSquared) {
        this.rSquared = rSquared;
    }

    public double[] getParameters() {
        return parameters;
    }

    public void setParameters(double[] parameters) {
        this.parameters = parameters;
    }

    public double[] getresult() {
        return result;
    }

    public void setresult(double[] result) {
        this.result = result;
    }

}
