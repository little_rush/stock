/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.File;
import javax.servlet.ServletContext;

/**
 * 分析介面
 *
 * @author little_rush
 */
public interface AnalyzingPlugin {

    public PredictLastDataResult analyze(ServletContext context, String stock, File csvFile) throws Exception;

}
