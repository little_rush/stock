    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import static java.lang.System.out;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 *亂數排列
 * @author little_rush
 */
public class Random {

    public ArrayList<String[]> execute(String[] listName) {

        String to[] = new String[listName.length];//9
        ArrayList<String[]> pear= new ArrayList();
        for (int i = 2; i <= listName.length; i++) {
            String str = comb(listName, to, i, listName.length, i);
            String[] tokens = str.split("\\n"); //切割跳行
            for (int k = 0; k < tokens.length; k++) {
                String[] token = tokens[k].split(",");//切割逗號
                String[] strData = new String[token.length];
                for (int j = 0; j < token.length; j++) {
                    if(token[j].equals("") || token == null){
                        
                    } else {
                        strData[j] = token[j];
                    }
                   
                }
                pear.add(strData);
            }
        }
        return pear;
    }

    //C(m, n) = C(m-1, n-1) + C(m-1, n)
    public static String comb(String[] listName, String[] to, int len, int m, int n) {
        String result = "";
        if (n == 0) {
            for (int i = 0; i < len; i++) {
                result += to[i] + ",";
            }
            result += "\n";
        } else {
            to[n - 1] = listName[m - 1];

            if (m > n - 1) {
                result = comb(listName, to, len, m - 1, n - 1);
            }
            if (m > n) {
                result = comb(listName, to, len, m - 1, n) + result;
            }
        }
        return result;

    }
}
