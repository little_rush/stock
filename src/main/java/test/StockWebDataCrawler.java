/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import com.google.gson.Gson;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;

/**
 *
 * @author lendle
 */
public class StockWebDataCrawler extends Thread {

    private StockWebDataCrawlerJobQueue queue = null;
    private boolean running = true;
    private ServletContext context = null;
    RimplementQueue rimplementQueue = null;

    public StockWebDataCrawler(ServletContext context, StockWebDataCrawlerJobQueue queue) {
        this.context = context;
        this.queue = queue;
        rimplementQueue = (RimplementQueue) context.getAttribute("rimplementQueue");
    }

    public void run() {
        int count=1;
        while (running) {
            String stockId = queue.getStock();
            if (stockId != null) {
                execks exe = new execks();
                try {
                    List ks = exe.exec(context, stockId);
                    if(ks.size()>600){
                        ks=ks.subList(ks.size()-600, ks.size());
                    }
                    //Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "stock", "stock");
                    Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "root", "stock");
                    Statement stmt=connection.createStatement();
                    stmt.executeUpdate("delete from stock where stock_id='"+stockId+"'");
                    PreparedStatement pstmt = connection.prepareStatement("insert into stock(stock_id,day,RSV,D,K,volume,MACD,OSC,DIF,close,open) values (?,?,?,?,?,?,?,?,?,?,?)");
                    Logger.getLogger(this.getClass().getName()).info("start crawl "+stockId);
                    for (int i = 0; i < ks.size(); i++) {
                        
                        Map map = (Map) ks.get(i);
                        if (map.containsKey("RSV") == false || map.get("RSV") == null || map.get("RSV").equals("null")) {
                            continue;
                        } else if (map.containsKey("D值") == false || map.get("D值") == null || map.get("D值").equals("null")) {
                            continue;
                        } else if (map.containsKey("K值") == false || map.get("K值") == null || map.get("K值").equals("null")) {
                            continue;
                        } else if (map.containsKey("MACD") == false || map.get("MACD") == null || map.get("MACD").equals("null")) {
                            continue;
                        } else if (map.containsKey("成交量") == false || map.get("成交量") == null || map.get("成交量").equals("null")) {
                            continue;
                        } else if (map.containsKey("收盤價") == false || map.get("收盤價") == null || map.get("收盤價").equals("null")) {
                            continue;
                        } else if (map.containsKey("OSC") == false || map.get("OSC") == null || map.get("OSC").equals("null")) {
                            continue;
                        } else if (map.containsKey("DIF") == false || map.get("DIF") == null || map.get("DIF").equals("null")) {
                            continue;
                        } else if (map.containsKey("開盤價") == false || map.get("開盤價") == null || map.get("開盤價").equals("null")) {
                            continue;
                        }
                        pstmt.setString(1, stockId);
                        pstmt.setString(2, (String) map.get("RPT_TIME"));
                        pstmt.setDouble(3, (Double) map.get("RSV"));
                        pstmt.setDouble(4, (Double) map.get("D值"));
                        pstmt.setDouble(5, (Double) map.get("K值"));
                        pstmt.setDouble(6, (Double) map.get("成交量"));
                        pstmt.setDouble(7, (Double) map.get("MACD"));
                        pstmt.setDouble(8, (Double) map.get("OSC"));
                        pstmt.setDouble(9, (Double) map.get("DIF"));
                        pstmt.setDouble(10, (Double) map.get("收盤價"));
                        pstmt.setDouble(11, (Double) map.get("開盤價"));
                        pstmt.addBatch();
                    }
                   pstmt.executeBatch();
                   connection.close();
                   ExportUtil.toCSV(stockId);
                   Logger.getLogger(this.getClass().getName()).info("add "+stockId+" to rimplementQueue");
                   rimplementQueue.addStock(stockId);
                } catch (Exception ex) {
                    Logger.getLogger(StockWebDataCrawler.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            try {
                if(count%5==0){
                    Thread.sleep(300000);
                }else{
                    Thread.sleep(30000);
                }
                count++;
            } catch (InterruptedException ex) {
                Logger.getLogger(StockWebDataCrawler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void shutdown() {
        this.running = false;
        this.interrupt();
    }
//
//    public static void main(String[] args) throws Exception {
//        StockWebDataCrawlerJobQueue queue = new StockWebDataCrawlerJobQueue();
//        final StockWebDataCrawler c = new StockWebDataCrawler(null, queue);
//        c.start();
//        Scanner s = new Scanner(System.in);
//        while (true) {
//            System.out.println("enter command=>");
//            String command = s.nextLine();
//            if (command.equals("-1")) {
//                c.shutdown();
//                break;
//            } else {
//                queue.addStock(command);
//            }
//        }
//    }
}
