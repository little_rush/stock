/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import rcaller.RCode;

/**
 *
 * @author treelazy
 */
public class Rimplement implements AnalyzingPlugin {

    @Override
    public PredictLastDataResult analyze(ServletContext context, String stock, File csvFile) throws Exception {
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "root", "stock");
        //Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "stock", "stock");
        PredictLastDataResult predictLastDatalist = new PredictLastDataResult();
//        RCode code = new RCode();
        try {
            RJ rj = new RJ("C:\\Program Files\\R\\R-3.2.2\\bin\\Rscript.exe");
//            code.addRCode("library(e1071, lib.loc='D:/')");
//            code.addRCode("install.packages('e1071', destdir='D:/', lib='D:/', repos='http://cran.us.r-project.org')");
            rj.appendRCode("source('C:/Users/treelazy/Downloads/ud.r')");
           // RJ rj = new RJ("/usr/bin/Rscript");
            //j.appendRCode("source('/home/little_rush/stock/ud.r')");
            rj.appendRCode("a<-predictLastData(read.csv('" + csvFile.getCanonicalPath().replace('\\', '/') + "'))");
            rj.setReturnedObjects(new String[]{"a"});
            Map map = rj.run();
            Map a = (Map) map.get("a");
            double nextClose = (Double) rj.getResultOGNL("a.testData.nextClose[0]");
            double close = (Double) rj.getResultOGNL("a.testData.close[0]");
            double repeatCount = (Double) rj.getResultOGNL("a.testData.repeatCount[0]");
            String today = (String) rj.getResultOGNL("a.testData.day[0]");
            predictLastDatalist.setClose(close);
            predictLastDatalist.setNextClose(nextClose);
            predictLastDatalist.setRepeatCount(repeatCount);
            predictLastDatalist.setToday(today);
            try {
                PreparedStatement pstmtpredictlastdata = connection.prepareStatement("insert into predictlastdata(stock_id,today,close,nextclose,repeatcount) values (?,?,?,?,?)");
                pstmtpredictlastdata.setString(1, stock);
                pstmtpredictlastdata.setString(2, today);
                pstmtpredictlastdata.setDouble(3, close);
                pstmtpredictlastdata.setDouble(4, nextClose);
                pstmtpredictlastdata.setDouble(5, repeatCount);
                pstmtpredictlastdata.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(AnalyzingDaemon.class.getName()).log(Level.SEVERE, null, ex);
            }
            rj.clearRCode();
        } finally {
            connection.close();
        }
        return predictLastDatalist;
    }

}
