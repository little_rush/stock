/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author treelazy
 */
public class CrawlStock extends Thread {

    private boolean running = true;
    StockWebDataCrawlerJobQueue stockWebDataCrawlerJobQueue = null;
    RimplementQueue rimplementQueue = null;
    private ServletContext servletContext = null;

    CrawlStock(ServletContext servletContext) {
        stockWebDataCrawlerJobQueue = (StockWebDataCrawlerJobQueue) servletContext.getAttribute("StockWebDataCrawlerJobQueue");
        rimplementQueue = (RimplementQueue) servletContext.getAttribute("rimplementQueue");
        this.servletContext = servletContext;
    }

    @Override
    public void run() {
        java.util.Date now = new java.util.Date();
        while (running) {
            try {
                Calendar c=Calendar.getInstance();
                if(c.get(Calendar.HOUR_OF_DAY)!=15){
                    Thread.sleep(30*60*1000);
                    continue;
                }
                HashMap<String, String> sentstock = (HashMap<String, String>) this.servletContext.getAttribute("sentstock");
                sentstock.clear();
                Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "root", "stock");
                //Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "stock", "stock");
                Statement st = connection.createStatement();
                Statement st1 = connection.createStatement();
//                List<String> lines = FileUtils.readLines(new File("/home/little_rush/stock/stockdataRSV.csv"));
                ResultSet rsuserchoose = st1.executeQuery("SELECT distinct stock_id FROM userchoose ;");
                while (rsuserchoose.next()) {
                    String stockId = rsuserchoose.getString("stock_id");
                    
                    ResultSet rs = st.executeQuery("SELECT max(day)as maxDay FROM stock WHERE stock_id='" + stockId + "';");
                    if (rs.next()) {
                        Date maxDay = rs.getDate("maxDay");
                        if (maxDay != null) {
                            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                            if (format.format(now).equals(format.format(maxDay)) == false) {
                                stockWebDataCrawlerJobQueue.addStock(stockId);
                                Logger.getLogger(this.getClass().getName()).info("add userchoose "+stockId);
                            }else{
                                rimplementQueue.addStock(stockId);
                                 Logger.getLogger(this.getClass().getName()).info("add analysis "+stockId);
                            }
                        } else {
                            stockWebDataCrawlerJobQueue.addStock(stockId);
                            Logger.getLogger(this.getClass().getName()).info("add userchoose maxDay=null "+stockId);
                        }
                    } else {
                        stockWebDataCrawlerJobQueue.addStock(stockId);
                        Logger.getLogger(this.getClass().getName()).info("add userchoose database null "+stockId);
                    }
                }
                connection.close();
                //Thread.sleep(12 * 60 * 60 * 1000);
            } catch (Exception ex) {
                Logger.getLogger(CrawlStock.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    public void shutdown() {
        this.running = false;
        this.interrupt();
    }
}
