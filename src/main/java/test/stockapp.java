/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.*;
import java.util.logging.Logger;
import javax.servlet.ServletContext;

/**
 *
 * @author little_rush
 */
public class stockapp {

    Calendar CD = Calendar.getInstance();
    private String time = null;

    public stockapp() {

    }

    public stockapp(ServletContext context, String stock) throws IOException, SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
       Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "root", "stock");
       // Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "stock", "stock");
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery("SELECT max(day)as maxDay FROM stock WHERE stock_id='" + stock + "';");
        java.util.Date now = new java.util.Date();
        if (rs.next()) {
            Date maxDay = rs.getDate("maxDay");
            //ExportUtil.toCSV(stock);
            if (maxDay != null) {
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                if (format.format(now).equals(format.format(maxDay)) == false) {
                    StockWebDataCrawlerJobQueue queue = (StockWebDataCrawlerJobQueue) context.getAttribute("StockWebDataCrawlerJobQueue");
                    queue.addStock(stock);
                } else {//查看資料庫是否有新的分析資料
                    ResultSet rspredictlastdata = st.executeQuery("SELECT max(today)as maxDay FROM predictlastdata WHERE stock_id='" + stock + "';");
                    if (rspredictlastdata.next() && rspredictlastdata.getDate("maxDay")!=null) {
                        if (format.format(now).equals(format.format(maxDay)) == false) {
                            RimplementQueue queue = (RimplementQueue) context.getAttribute("rimplementQueue");
                            queue.addStock(stock);
                        }
                    } else {
                        RimplementQueue queue = (RimplementQueue) context.getAttribute("rimplementQueue");
                        queue.addStock(stock);
                    }
                }
            }

        } else {
            StockWebDataCrawlerJobQueue queue = (StockWebDataCrawlerJobQueue) context.getAttribute("StockWebDataCrawlerJobQueue");
            queue.addStock(stock);
        }
        connection.close();
    }
}
