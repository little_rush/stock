//package test;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.OutputStreamWriter;
//import java.io.PrintWriter;
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import ognl.OgnlException;
//import org.apache.commons.io.FileUtils;
//import test.PredictLastDataResult;
//import test.RJ;
//import test.RJException;
//import test.RegressionResult;
//import test.svmregression;
//
//public class PredictLastData {
//    private ArrayList<PredictLastDataResult> predictLastDataresult = new ArrayList<PredictLastDataResult>();
//
//    public ArrayList<PredictLastDataResult> getPredictLastDataresult() {
//        return predictLastDataresult;
//    }
//    
//    
//    /**
//     * @param args the command line arguments
//     */
//    ArrayList<String> stockname = new ArrayList<String>();
//    public PredictLastData(String stock) throws ClassNotFoundException, SQLException, IOException, InterruptedException, RJException, OgnlException {
//        
//        PredictLastDataResult predictLastDatalist = new PredictLastDataResult();
//        PrintWriter output = new PrintWriter(new OutputStreamWriter(new FileOutputStream("/home/little_rush/stock/result.csv"), "utf-8"));
//        Class.forName("com.mysql.jdbc.Driver");
//        //Connection connection = DriverManager.getConnection("jdbc:mysql://ilab.twgogo.org/stock", "stock", "stock");
//        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "stock", "stock");//亦陽的測試
//        //Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "root", "stock");
//        Statement st = connection.createStatement();//Statement指令
//        ResultSet rs = st.executeQuery("SELECT * FROM stock WHERE stock_id='" + stock + "' ORDER BY day desc limit 0,300 ;");
//        PrintWriter writer = new PrintWriter(new FileWriter("/home/little_rush/stock/" + stock + ".csv"));
//        writer.write("day" + "," + "close" + "," + "open" + "," + "volume" + "," + "DIF" + "," + "OSC" + "," + "RSV" + "," + "D" + "," + "K" + "," + "MACD" + "\n");
//        while (rs.next()) {
//            HashMap mapRecords = new HashMap();
//            mapRecords.put("MACD", rs.getDouble("MACD")); //前面放Key值,後面放資料庫的值
//            mapRecords.put("K值", rs.getDouble("K"));
//            mapRecords.put("D值", rs.getDouble("D"));
//            mapRecords.put("RSV", rs.getDouble("RSV"));
//            mapRecords.put("OSC", rs.getDouble("OSC"));
//            mapRecords.put("DIF", rs.getDouble("DIF"));
//            mapRecords.put("成交量", rs.getDouble("volume"));
//            mapRecords.put("收盤價", rs.getDouble("close"));
//            mapRecords.put("RPT_TIME", rs.getString("day"));
//            writer.write(rs.getString("day") + "," + rs.getDouble("close") + "," + rs.getDouble("open") + "," + rs.getDouble("volume") + "," + rs.getDouble("DIF") + "," + rs.getDouble("OSC") + "," + rs.getDouble("RSV") + "," + rs.getDouble("D") + "," + rs.getDouble("K") + "," + rs.getDouble("MACD") + "\n");
//        }
//        writer.close();
//        connection.close();
//        RJ rj = new RJ("/usr/bin/Rscript");
//
//        rj.appendRCode("source('/home/little_rush/stock/ud.r')");
//        List<String> lines = FileUtils.readLines(new File("/home/little_rush/stock/stockdataRSV.csv"));
//        try {
////            for (int j = (lines.size()-10); j < lines.size(); j++) {
////                String[] line = lines.get(j).split(",");
////                String stockCategory = line[1].trim();
//                rj.appendRCode("a<-predictLastData(read.csv('/home/little_rush/stock/" + stock + ".csv'))");
//
//                rj.setReturnedObjects(new String[]{"a"});
//                Map map = rj.run();
//
//                Map a = (Map) map.get("a");
//                System.out.println(a);
//                double nextClose = (Double) rj.getResultOGNL("a.testData.nextClose[0]");
//                double close = (Double) rj.getResultOGNL("a.testData.close[0]");
//                double utype = (Double) rj.getResultOGNL("a.testData.utype[0]");
//                double repeatCount = (Double) rj.getResultOGNL("a.testData.repeatCount[0]");
//                predictLastDatalist.setClose(close);
//                predictLastDatalist.setNextClose(nextClose);
//                predictLastDatalist.setRepeatCount(repeatCount);
//                predictLastDatalist.setUtype(utype);
//                predictLastDataresult.add(predictLastDatalist);
//                output.print(stock + ","  + nextClose + "," + close + "," + utype + "," + repeatCount);
////                System.out.println("nextClose" + nextClose);
////                System.out.println("close" + close);
////                System.out.println("utype" + utype);
////                System.out.println("repeatCount" + repeatCount);
//                rj.clearRCode();
////            }
//        } finally {
//            output.close();
//        }
//    }
//
////    public static void main(String[] args) throws Exception {
////        List<String> lines = FileUtils.readLines(new File("C:\\Users\\treelazy\\Desktop\\stockdataRSV.csv"));
////        for (int i = (lines.size()-10); i < lines.size(); i++) {
////            String[] line = lines.get(i).split(",");
////            String stockId = line[0].trim();
////            new PredictLastData(stockId);
////        }
////        if (1 == 1) {
////            return;
////        }
////
////    }
//}
