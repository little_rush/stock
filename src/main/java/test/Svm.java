///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package test;
//
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.ResultSet;
//import java.sql.Statement;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import libsvm.svm;
//import libsvm.svm_model;
//import libsvm.svm_node;
//import libsvm.svm_parameter;
//import libsvm.svm_problem;
//
///**
// * SVM分析
// *
// * @author sofa
// */
//public class Svm implements AnalyzingPlugin {
//
//    public double[] temp = null;
//
//    @Override
//    public List<AnalyzingResult> execute(String stock, List<StockIndicators> dataList, double[] y) {
//        try {
//            Class.forName("com.mysql.jdbc.Driver");
//           // Connection connection = DriverManager.getConnection("jdbc:mysql://ilab.twgogo.org/stock", "stock", "stock");
//            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "stock", "stock");//亦陽的測試
//            //Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "root", "stock");
//            Statement st = connection.createStatement();//Statement指令
//            
//            ResultSet rs1 = st.executeQuery("SELECT count(*) FROM svmgreatresult WHERE stock_id='" + stock + "';");
//            rs1.next();
//
//            svm_problem prob = new svm_problem();
//            ArrayList<AnalyzingResult> svmanalyzingdata = new ArrayList<AnalyzingResult>();
//            AnalyzingResult ar = new AnalyzingResult();
//            prob.x = new svm_node[dataList.size()][rs1.getInt(1)];
//            prob.l = dataList.size() - 1;
//            prob.y = new double[dataList.size()];
//            ResultSet rs = st.executeQuery("SELECT * FROM svmgreatresult WHERE stock_id='" + stock + "';");
//            List<String> fields = new ArrayList<String>();
//            while (rs.next()) {
//                fields.add(rs.getString("Fields_name"));
//            }
//            for (int i = 1; i < dataList.size(); i++) {
//                for (int j = 0; j < fields.size(); j++) {
//                    svm_node node0 = new svm_node();
//                    node0.index = (j + 1);
//                    node0.value = dataList.get(i).getData(fields.get(j));
//                    prob.x[i - 1][j] = node0;
//                }
//                StockIndicators keyname = dataList.get(i);
//                StockIndicators keyname1 = dataList.get(i - 1);//昨收價
//
//                if (keyname.getData("close") > keyname1.getData("close")) {
//                    prob.y[i - 1] = 1;//漲  
//                } else {
//                    prob.y[i - 1] = 0;//跌
//                }
//            }
//
//            svm_parameter param = new svm_parameter();
//            param.probability = 1;
//            param.gamma = 0.5;
//            param.nu = 0.5;
//            param.C = 100;
//            param.svm_type = svm_parameter.NU_SVC;
//            param.kernel_type = svm_parameter.RBF;
//            param.cache_size = 20000;
//            param.eps = 0.001;
//
//            svm_model model = svm.svm_train(prob, param);
//            StockIndicators last = dataList.get(dataList.size() - 1);
//            svm_node [] test=new svm_node[fields.size()];
//            double[] prob_estimates = new double[2];
//            //for (int i = 1; i < dataList.size(); i++) {
//                for (int j = 0; j < fields.size(); j++) {
//                    svm_node node0 = new svm_node();
//                    node0.index = (j + 1);
//                    node0.value = last.getData(fields.get(j));
//                    test[j]=node0;
//                }
//
//            //}
//
//            svm.svm_predict_probability(model, test, prob_estimates);
//            /*System.out.println(Arrays.toString(prob_estimates));*/
//            temp = prob_estimates;
//            double max = Math.max(prob_estimates[0], prob_estimates[1]);
//            double min = Math.min(prob_estimates[0], prob_estimates[1]);
//            double conf = (max - min) / max;
//            ar.setConfidence(conf);
//            ar.setDate(dataList.get(dataList.size() - 1).getNowday());
//            ar.setRise(prob_estimates[1] > prob_estimates[0]);
//            ar.setEstimate(prob_estimates);
//            svmanalyzingdata.add(ar);
//            return svmanalyzingdata;
//        } catch (Exception e) {
//            e.printStackTrace();
//            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
//        }
//        return null;
//    }
//}
