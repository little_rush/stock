/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author treelazy
 */
public class RimplementQueue {
    private List<String> queuedId = new ArrayList<String>();

    public synchronized void addStock(String id) {
        queuedId.add(id);
        this.notifyAll();
    }

    public synchronized String getStock() {
        try {
            while (queuedId.isEmpty()) {
                this.wait();
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(StockWebDataCrawlerJobQueue.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (queuedId.isEmpty()) {
            return null;
        }
        String stockId = queuedId.remove(0);
        return stockId;
    }

    public List<String> getQueueContent() {
        return new ArrayList<String>(queuedId);
    }
}
