/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import ognl.Ognl;
import ognl.OgnlException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author lendle
 */
public class RJ {
    private static boolean first=true;
    private String rscriptPath = null;
    //private String rCode = "library(RJSONIO)";
    private String rCode = ".libPaths('D:/')\nlibrary(RJSONIO, lib.loc='D:/')";
    private Map result = null;
    private String[] returnedObjects = null;

    public RJ(String rscriptPath) {
        this.rscriptPath = rscriptPath;
    }

    public String getRscriptPath() {
        return rscriptPath;
    }

    public void setRscriptPath(String rscriptPath) {
        this.rscriptPath = rscriptPath;
    }

    public String getrCode() {
        return rCode;
    }

    public String[] getReturnedObjects() {
        return returnedObjects;
    }

    public void setReturnedObjects(String[] returnedObjects) {
        this.returnedObjects = returnedObjects;
    }

    public void appendRCode(String code) {
        rCode = rCode + "\n" + code;
    }

    public void clearRCode() {
        //this.rCode = "library(RJSONIO)";
        this.rCode = ".libPaths('D:/')\nlibrary(RJSONIO, lib.loc='D:/')";
    }

    public Map run() throws IOException, InterruptedException, RJException {
        if(first){
            //this.rCode="install.packages('RJSONIO', destdir='D:/', lib='D:/', repos='http://cran.us.r-project.org')"+"\n"+this.rCode;
            first=true;
        }
        File resultFile = File.createTempFile("rjBridge", ".tmp.json");
        if (this.returnedObjects != null) {
            this.appendRCode("rjReturn<-list()");
            for (String obj : this.returnedObjects) {
                this.appendRCode("rjReturn$" + obj + "<-" + obj);
            }
            this.appendRCode("write(toJSON(rjReturn), '" + resultFile.getCanonicalPath().replace('\\', '/') + "')");
        }
        File tempScriptFile = File.createTempFile("rjBridge", ".tmp.R");
        FileUtils.write(tempScriptFile, this.rCode);
        System.out.println(this.rCode);
        ProcessBuilder pb = new ProcessBuilder(this.rscriptPath, tempScriptFile.getCanonicalPath());
        pb.redirectErrorStream(true);
        Process p = pb.start();
        int ret = p.waitFor();
        try {
            if(ret!=0){
                //error
                RJException e=new RJException(IOUtils.toString(p.getInputStream(), "big5"));
                throw e;
            }
            if (this.returnedObjects != null && this.returnedObjects.length > 0) {
                String str = FileUtils.readFileToString(resultFile);
                this.result = new Gson().fromJson(str, Map.class);
            }
        } finally {
            FileUtils.deleteQuietly(resultFile);
            FileUtils.deleteQuietly(tempScriptFile);
        }
        //System.out.println(IOUtils.toString(p.getInputStream(), "utf-8"));

        return this.result;
    }

    public Map getResult() {
        return result;
    }
    
    public Object getResultOGNL(String expression) throws OgnlException{
        return Ognl.getValue(expression, this.result);
    }
}
