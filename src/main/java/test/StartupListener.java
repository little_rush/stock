/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Web application lifecycle listener.
 *
 * @author treelazy
 */
public class StartupListener implements ServletContextListener {

    private StockWebDataCrawlerJobQueue stockWebDataCrawlerJobQueue = new StockWebDataCrawlerJobQueue();
    private RimplementQueue rimplementQueue = new RimplementQueue();
    private SendemailQeeue sendemailQeeue = new SendemailQeeue();
    private StockWebDataCrawler stockWebDataCrawler = null;
    private AnalyzingDaemon rimplementstock = null;
    private AnalyzingDaemon rimplementstock1 = null;
    private Sendemail sendemail = null;
    private CrawlStock crawlStock = null;
    private HashMap<String, String> sentstock = new HashMap<String, String>();

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(StartupListener.class.getName()).log(Level.SEVERE, null, ex);
        }
        Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF);
        Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
        sce.getServletContext().setAttribute("StockWebDataCrawlerJobQueue", stockWebDataCrawlerJobQueue);
        sce.getServletContext().setAttribute("rimplementQueue", rimplementQueue);
        sce.getServletContext().setAttribute("SendemailQeeue", sendemailQeeue);
        sce.getServletContext().setAttribute("sentstock", sentstock);
        this.stockWebDataCrawler = new StockWebDataCrawler(sce.getServletContext(), this.stockWebDataCrawlerJobQueue);
        this.rimplementstock = new AnalyzingDaemon(sce.getServletContext(), this.rimplementQueue);
        this.rimplementstock1 = new AnalyzingDaemon(sce.getServletContext(), this.rimplementQueue);
        this.sendemail = new Sendemail(sce.getServletContext(), this.sendemailQeeue);
        crawlStock = new CrawlStock(sce.getServletContext());
        this.stockWebDataCrawler.start();
        this.rimplementstock.start();
        this.rimplementstock1.start();
        this.sendemail.start();
        crawlStock.start();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        this.stockWebDataCrawler.shutdown();
        this.rimplementstock.shutdown();
        this.sendemail.shutdown();
        crawlStock.shutdown();
    }
}
