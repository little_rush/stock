/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import rcaller.RCaller;
import rcaller.RCode;

/**
 *
 * @author treelazy
 */
public class svmregression {

    public double svmresult(String stock) throws SQLException, IOException {
        RCaller caller = new RCaller();
        RCode code = new RCode();
        code.clear();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            //Connection connection = DriverManager.getConnection("jdbc:mysql://ilab.twgogo.org/stock", "stock", "stock");
            //Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "stock", "stock");//亦陽的測試
           Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "root", "stock");
            Statement st = connection.createStatement();//Statement指令
            ResultSet rs = st.executeQuery("select * from stock where stock_id ='" + stock + "'");
            PrintWriter writer = new PrintWriter(new FileWriter("/home/little_rush/stock/" + stock + ".csv"));
            writer.write("day" + "," + "close" + "," + "open" + "," + "volume" + "," + "DIF" + "," + "OSC" + "," + "RSV" + "," + "D" + "," + "K" + "," + "MACD" + "\n");
            while (rs.next()) {
                HashMap mapRecords = new HashMap();
                mapRecords.put("MACD", rs.getDouble("MACD")); //前面放Key值,後面放資料庫的值
                mapRecords.put("K值", rs.getDouble("K"));
                mapRecords.put("D值", rs.getDouble("D"));
                mapRecords.put("RSV", rs.getDouble("RSV"));
                mapRecords.put("OSC", rs.getDouble("OSC"));
                mapRecords.put("DIF", rs.getDouble("DIF"));
                mapRecords.put("成交量", rs.getDouble("volume"));
                mapRecords.put("收盤價", rs.getDouble("close"));
                mapRecords.put("RPT_TIME", rs.getString("day"));
                writer.write(rs.getString("day") + "," + rs.getDouble("close") + "," + rs.getDouble("open") + "," + rs.getDouble("volume") + "," + rs.getDouble("DIF") + "," + rs.getDouble("OSC") + "," + rs.getDouble("RSV") + "," + rs.getDouble("D") + "," + rs.getDouble("K") + "," + rs.getDouble("MACD") + "\n");
            }
            writer.close();
            connection.close();

            String listName[] = {"RSV", "D", "K", "volume", "MACD", "OSC", "DIF"/*, "fivema"*/};
            Random rd = new Random();
            ArrayList<String[]> strList = rd.execute(listName);
            caller.setRscriptExecutable("/usr/bin/Rscript");
            caller.setRExecutable("C:\\Program Files\\R\\R-3.2.2\\bin\\R.exe");
            caller.cleanRCode();
            //code.addRCode("install.packages('e1071', destdir='D:/', lib='D:/', repos='http://cran.us.r-project.org')");//安裝e1071
            code.addRCode("library(e1071, lib.loc='D:/')");
            code.addRCode("source('C:/Users/treelazy/Downloads/ud.r')");
            code.addRCode("result<-udprep(read.csv('C:/Users/treelazy/Documents/NetBeansProjects/" + stock + ".csv'))");
            //code.addRCode("write.csv(result$data,file='C:/Users/treelazy/Documents/NetBeansProjects/1339.csv')");
//            code.addRCode("data<-read.csv('C:/Users/treelazy/Documents/NetBeansProjects/" + stock + ".csv')");
//            for (String[] strData : strList) {
//                String listNamea = "";
//
//                for (int j = 0; j < strData.length; j++) {
//                    out.println("strData:" + strData[j]);
//                    listNamea += "'" + strData[j] + "'";
//                    if (j != strData.length - 1) {
//                        listNamea += ",";
//                    }
//
//                }
//                code.addRCode("result<-svmtest(data, c("+listNamea+"), closeColumn=\"close\", svmType=\"nu-classification\")");
//                code.addRCode("result<-fullPredict(data,closeColumn=\"close\")");
//                code.addRCode("model<-result$lmModel");
                //code.addRCode("names(result)");
                //code.addRCode("x<-2");
                caller.setRCode(code);
                caller.redirectROutputToStream(System.out);
                caller.runAndReturnResultOnline("result");
                //caller.runAndReturnResult("result");
                caller.runOnly();
                //out.println(caller.getParser().getNames());
                //caller.runAndReturnResultOnline("model");
                out.println(Arrays.toString(caller.getParser().getAsDoubleArray("result")));
                caller.stopStreamConsumers();
                caller.StopRCallerOnline();
                
                //out.println("listNamea：" + listNamea);
                //break;
            //}

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(svmregression.class.getName()).log(Level.SEVERE, null, ex);
        }
//        return (caller.getParser().getAsDoubleArray("result")[0]);
        return 0;
    }
    
    public static void main(String [] args) throws Exception{
        new svmregression().svmresult("2008");
    }
}
