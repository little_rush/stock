/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.jetty.util.log.Log;

/**
 *股票所有的值
 * @author little_rush
 */
public class StockIndicators {

    private HashMap data;
    private String nowday;

    public String getNowday() {
        return nowday;
    }

    public void setNowday(String nowday) {
        this.nowday = nowday;
    }
    public StockIndicators(){
        data = new HashMap();
    }

    public double getData(String dataName) {
        System.out.println("dataName="+dataName+", "+data.get(dataName));
        double datamath = (Double) data.get(dataName);
        return datamath;
    }

    public void setRSV(double RSV) {
        data.put("RSV", RSV);
    }

    public void setD(double D) {
        data.put("D", D);
    }

    public void setK(double K) {
        data.put("K", K);
    }

    public void setVolume(double volume) {
        data.put("volume", volume);
    }

    public void setMACD(double MACD) {
        data.put("MACD", MACD);
    }

    public void setOSC(double OSC) {
        data.put("OSC", OSC);
    }

    public void setDIF(double DIF) {
        data.put("DIF", DIF);
    }
    
    public void setClose(double close){
        data.put("close", close);
    }
}
