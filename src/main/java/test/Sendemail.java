/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.IOException;
import java.net.Authenticator;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;

/**
 *
 * @author treelazy
 */
public class Sendemail extends Thread {

    private ServletContext servletContext = null;
    private SendemailQeeue sendemailQeeue = null;
    private boolean running = true;
    Calendar CD = Calendar.getInstance();

    Sendemail(ServletContext servletContext, SendemailQeeue sendemailQeeue) {
        this.servletContext = servletContext;
        this.sendemailQeeue = (SendemailQeeue) servletContext.getAttribute("SendemailQeeue");
    }

    public void run() {
        while (running) {
            String stock = sendemailQeeue.getStock();
            Logger.getLogger(this.getClass().getName()).info("add stock to sentstock" + stock);
            if (stock != null) {
                Transport transport=null;
                try {
                  Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "root", "stock");
                   //Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "stock", "stock");
                    Statement st = connection.createStatement();
                    HashMap<String, String> sentstock = (HashMap<String, String>) this.servletContext.getAttribute("sentstock");
                    ResultSet rs = st.executeQuery("SELECT max(predictlastdata.today)as maxday, member.email,userchoose.stock_id,userchoose.name,predictlastdata.close,predictlastdata.nextclose,predictlastdata.repeatcount FROM userchoose LEFT OUTER JOIN member ON userchoose.name=member.name LEFT OUTER JOIN predictlastdata ON userchoose.stock_id=predictlastdata.stock_id WHERE predictlastdata.stock_id='" + stock + "' group by member.email");
                    String user = "treelazy821006@gmail.com";
                    String pwd = "treelazy821006";
                    String from = "treelazy821006@gmail.com";
                    
                    Properties props = new Properties();
                    props.put("mail.smtp.auth", "true");
                    props.put("mail.smtp.starttls.enable", "true");
                    props.put("mail.smtp.host", "smtp.gmail.com");
                    props.put("mail.smtp.port", "587");
                    useremail authentication = new useremail(user, pwd);
                    Session mailSession = Session.getInstance(props, authentication);
                    transport = mailSession.getTransport("smtp");
                    transport.connect(null, user, pwd);
                    while (rs.next()) {
                        String to = rs.getString("member.email");
                        String userstock = rs.getString("userchoose.name");
                        sentstock.put(userstock, stock);
                        Logger.getLogger(this.getClass().getName()).info("add stock to sentstockMap" + userstock + "會員" + stock + " / " + rs.isLast());
                        if (sentstock.get(stock) == stock && sentstock.get(userstock) == userstock) {
                            Logger.getLogger(this.getClass().getName()).info("stock already sent");
                        } else {
                            try {
                                MimeMessage message = new MimeMessage(mailSession);
                                message.setFrom(new InternetAddress(from));
                                message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
                                message.setSubject("親愛的會員您好");
                                StringBuffer html = new StringBuffer();
                                html.append("<h3>您所選的股票" + stock + "已經分析完成了</h3><br>");
                                html.append("<table border=2 width=200px><tr><td>股票名稱：" + stock + "</td></tr><tr><td>日期：" + rs.getString("maxday") + "</td</tr><tr><td>今天收盤價：" + rs.getDouble("predictlastdata.close") + "</td></tr><tr><td>明天收盤價：" + rs.getDouble("predictlastdata.nextclose") + "</td></tr></table><br>");
                                html.append("<p>連結為<a href=\"http://ilab1.twgogo.org:8080/searchstock/\">歡迎光臨股票系統</a></p><br>");
                                message.setContent(html.toString(), "text/html; charset=UTF-8");

                                transport.sendMessage(message, message.getAllRecipients());
                                Logger.getLogger(this.getClass().getName()).info("send email finish!");
                            } catch (MessagingException mex) {
                                mex.printStackTrace();
                            }
                        }
                    }
                    connection.close();
                } catch (Exception ex) {
                    Logger.getLogger(Sendemail.class.getName()).log(Level.SEVERE, null, ex);
                }finally{
                    if(transport!=null){
                        try {
                            transport.close();
                        } catch (MessagingException ex) {
                            Logger.getLogger(Sendemail.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                
            }
        }
    }

    public void shutdown() {
        this.running = false;
        this.interrupt();
    }
}
