<%@page import="java.util.Map"%>
<%@page import="org.apache.commons.math3.stat.descriptive.moment.StandardDeviation"%>
<%@page import="java.util.Arrays"%>
<%@page import="test.Svm"%>
<%@page import="test.RegressionResult"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="test.stockapp"  %>
<%@ page import="test.RegressionResult" %>
<%@page import="test.Random"%>
<%@page import="test.implementstock"%>
<%@page import="test.StockIndicators"%>
<%@page import="test.AnalyzingResult"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>hahaha welcom to stock family</title>

    </head>
    <body>
        <form action="app.java" method="post">
            <%
                request.setCharacterEncoding("UTF-8");

//                String[] stocks = new String[]{"8261", "2436", "8222", "1459", "2603", "1460", "2308", "1504", "2379"}; //前25% 宏全 華碩 寶隆 中間50% 台朔化 宏益 精誠 後25% 三發 喬福 祥碩
//                for (String stock : stocks) {
                HashMap sessionresult = new HashMap();
                HashMap stockresult = new HashMap();
                String stock = request.getParameter("stock");
                stockresult.put("stockname", stock);
                sessionresult.put("stockname", stock);

                session.setAttribute("stockname", stock);
                implementstock r = new implementstock();
                stockapp stockdata = new stockapp(application, stock);
                List<AnalyzingResult> arresult = r.execute(stockdata.getX(), stockdata.getY());
                Svm svm = new Svm();
                List<AnalyzingResult> svmResult = svm.execute(stockdata.getX(), stockdata.getY());
                double[] degreeArray = new double[svmResult.size()];//比值
                int[] signalArray = new int[svmResult.size()];//信號
                double sum = 0;//總和
                for (int i = 0; i < svmResult.size(); i++) {
                    /*String [] array=new String[]{""+stockdata.getX().get(i).getData("RSV"),
                     ""+stockdata.getX().get(i).getData("D"),
                     ""+stockdata.getX().get(i).getData("K"),
                     ""+stockdata.getX().get(i).getData("volume"),
                     ""+stockdata.getX().get(i).getData("MACD"),
                     ""+stockdata.getX().get(i).getData("OSC"),
                     ""+stockdata.getX().get(i).getData("DIF"),
                     ""+stockdata.getX().get(i).getData("close"),
                     ""+stockdata.getY()[i]};
                     out.println(String.join(",", array)+"<br/>");*/
                    degreeArray[i] = svmResult.get(i).getEstimate()[1] / svmResult.get(i).getEstimate()[0];
                    sum += degreeArray[i];
                }
                StandardDeviation std = new StandardDeviation();//標準差
                double stdValue = std.evaluate(degreeArray);//標準差
                double mean = sum / degreeArray.length;//平均值
                int index = 0;
                //信號
                for (double degree : degreeArray) {
                    if ((degree - mean) >= (mean - 0.5 * stdValue) && (degree - mean) <= (mean + 0.5 * stdValue)) {
                        signalArray[index] = 1;
                    } else if ((degree - mean) < (mean - 0.5 * stdValue)) {
                        signalArray[index] = 0;
                    } else {
                        signalArray[index] = 2;
                    }
                    index++;
                }

                out.println("<table border=1 align=left>");
                out.println("<tr>");
                out.println("<td>" + "股票:" + stock + "</td>");
                out.println("</tr>");
                out.println("<tr>");
                out.println("<td>" + Arrays.toString(svm.temp) + "</td>");
                session.setAttribute("svm", Arrays.toString(svm.temp));
                sessionresult.put("svm", Arrays.toString(svm.temp));
                out.println("</tr>");
                out.println("<tr>");
                out.println("<td>" + svmResult.get(0).getRise() + ":" + svmResult.get(0).getConfidence() + "</td>");
                sessionresult.put("ResultandtConfidence", svmResult.get(0).getRise() + ":" + svmResult.get(0).getConfidence());
                session.setAttribute("ResultandtConfidence", svmResult.get(0).getRise() + ":" + svmResult.get(0).getConfidence());
                out.println("</tr>");
//                    out.println("<tr>");
//                    out.println("<td>" + "SVM:" + Arrays.toString(svm.temp) + "</td>");
//                    out.println("</tr>");
                StockIndicators keyname = stockdata.getLastData();
//                out.println("RSV:" + keyname.getData("RSV") + "<br>");
//                out.println("D:" + keyname.getData("D") + "<br>");
//                out.println("K:" + keyname.getData("K") + "<br>");
//                out.println("volume:" + keyname.getData("volume") + "<br>");
//                out.println("MACD:" + keyname.getData("MACD") + "<br>");
//                out.println("OSC:" + keyname.getData("OSC") + "<br>");
//                out.println("DIF:" + keyname.getData("DIF") + "<br>"); 
                int k = 0;
                out.println("<tr>");
                out.println("<td>" + "Day:" + keyname.getNowday() + "</td>");
                sessionresult.put("day", keyname.getNowday());
                session.setAttribute("day", keyname.getNowday());
                out.println("</tr>");
                out.println("<tr>");
                out.println("<td>" + "close:" + keyname.getData("close") + "</td>");
                sessionresult.put("close", keyname.getData("close"));
                session.setAttribute("close", keyname.getData("close"));
                out.println("</tr>");
                for (int i = 0; i < arresult.size(); i++) {
                    RegressionResult regressionResult = (RegressionResult) arresult.get(k);
                    double rSquared = arresult.get(k).getConfidence();
                    double total = arresult.get(k).getValue();
                    if (total > keyname.getData("close") && rSquared > 0.6) {
                        out.println("<tr>");
                        out.print("<td>" + "總和 :" + total + "</td>");
                        sessionresult.put("sum", total);
                        session.setAttribute("sum", total);
                        out.println("</tr>");
                        double parameters[] = regressionResult.getParameters();
                        out.println("<tr>");
                        out.println("<td>");
                        for (String strData1 : regressionResult.getFields()) {
                            sessionresult.put("strData1", regressionResult.getFields());
                            out.print(strData1 + "   ");
                            session.setAttribute("strData1", strData1 + "   ");
                        }
                        out.println("</td>");
                        out.println("</tr>");
                        out.println("<tr>");
                        out.println("<td>" + "rSquared:" + rSquared + "</td>");
                        sessionresult.put("rSquared", rSquared);
                        session.setAttribute("rSquared", rSquared);
                        out.println("</tr>");
//                    out.println("<br>");
//                    out.println("Regression parameters: ");
//                    for (int j = 0; j < parameters.length; j++) {
//                        out.println(parameters[j] + "</br>");
//                    }
                        k++;
                    } else if (total < keyname.getData("close") && rSquared > 0.6) {
                        out.println("<tr>");
                        out.print("<td>" + "總和 :" + total + "</td>");
                        sessionresult.put("sum", total);
                        session.setAttribute("sum", total);
                        out.println("</tr>");
                        double parameters[] = regressionResult.getParameters();
                            out.println("<tr>");
                        out.println("<td>");
                        sessionresult.put("strData1", regressionResult.getFields());
                        for (String strData1 : regressionResult.getFields()) {

                            out.print(strData1 + "   ");
                            session.setAttribute("strData1", strData1 + "   ");
                        }
                        out.println("</td>");
                        out.println("</tr>");
                        out.println("<tr>");
                        out.println("<td>" + "rSquared:" + rSquared + "</td>");
                        session.setAttribute("rSquared", rSquared);
                        sessionresult.put("rSquared", rSquared);
                        out.println("</tr>");
//                    out.println("<br>");
//                    out.println("Regression parameters: ");
//                    for (int j = 0; j < parameters.length; j++) {
//                        out.println(parameters[j] + "</br>");
//                    }
                        k++;
                    }
                }
                out.println("</table>");
//                }
                //
                stockresult.putAll(sessionresult);
                session.setAttribute("allstockresult", stockresult);
                response.sendRedirect("result.jsp");
            %>
        </form>
    </body>
</html>
