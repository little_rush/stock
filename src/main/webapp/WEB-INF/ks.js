var stockData=null;

function StockChart(){
    
}

function Painter(oStockChart, StockData){
    stockData=StockData;
    this.Paint=function(){}
}
function _InitStockChart(oParms) {
	// 定義圖形初始參數
    InitStockChart({CanvasID:'StockCanvas'});
    var StockData = stockData;
    var ChartConfig = LoadChartConfig('{    "Global": {        "InitIndexLength": 120,        "ChartItems": {            "KLine": {                "MA_DevRatioRef": "PriceClose",                "MA_ShowTouchTag": "T",                "PaintItems": {                    "KLine": {                        "ShowHighLow": "F",                        "BandTag": {                            "IsShow": "F",                            "Cycle_DATE": 62,                            "Cycle_WEEK": 13,                            "Cycle_MONTH": 12,                            "Cycle_QUAR": 4,                            "Cycle_YEAR": 3                        }                    },                    "MA_DATE0": {                        "Name": "5日",                        "Count": 5,                        "Color": "#808080",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_DATE1": {                        "Name": "10日",                        "Count": 10,                        "Color": "#007D48",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_DATE2": {                        "Name": "月",                        "Count": 21,                        "Color": "#FF0000",                        "LineWidth": 1,                        "IsShow": "T"                    },                    "MA_DATE3": {                        "Name": "季",                        "Count": 62,                        "Color": "#FF8000",                        "LineWidth": 1,                        "IsShow": "T"                    },                    "MA_DATE4": {                        "Name": "半年",                        "Count": 124,                        "Color": "#00E600",                        "LineWidth": 1,                        "IsShow": "T"                    },                    "MA_DATE5": {                        "Name": "年",                        "Count": 248,                        "Color": "#0080FF",                        "LineWidth": 1,                        "IsShow": "T"                    },                    "MA_DATE6": {                        "Name": "三年",                        "Count": 745,                        "Color": "#FF0080",                        "LineWidth": 1,                        "IsShow": "T"                    },                    "MA_DATE7": {                        "Name": "五年",                        "Count": 1242,                        "Color": "#1919B3",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_DATE8": {                        "Name": "十年",                        "Count": 2484,                        "Color": "#8000FF",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_WEEK0": {                        "Name": "月",                        "Count": 4,                        "Color": "#ff0000",                        "LineWidth": 1,                        "IsShow": "T"                    },                    "MA_WEEK1": {                        "Name": "季",                        "Count": 13,                        "Color": "#ff8000",                        "LineWidth": 1,                        "IsShow": "T"                    },                    "MA_WEEK2": {                        "Name": "半年",                        "Count": 26,                        "Color": "#00e600",                        "LineWidth": 1,                        "IsShow": "T"                    },                    "MA_WEEK3": {                        "Name": "年",                        "Count": 52,                        "Color": "#0080ff",                        "LineWidth": 1,                        "IsShow": "T"                    },                    "MA_WEEK4": {                        "Name": "三年",                        "Count": 156,                        "Color": "#ff0080",                        "LineWidth": 1,                        "IsShow": "T"                    },                    "MA_WEEK5": {                        "Name": "五年",                        "Count": 260,                        "Color": "#1919b3",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_WEEK6": {                        "Name": "十年",                        "Count": 521,                        "Color": "#8000ff",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_WEEK7": {                        "Name": "",                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_WEEK8": {                        "Name": "",                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_MONTH0": {                        "Name": "半年",                        "Count": 6,                        "Color": "#00E600",                        "LineWidth": 1,                        "IsShow": "T"                    },                    "MA_MONTH1": {                        "Name": "年",                        "Count": 12,                        "Color": "#0080FF",                        "LineWidth": 1,                        "IsShow": "T"                    },                    "MA_MONTH2": {                        "Name": "三年",                        "Count": 36,                        "Color": "#FF0080",                        "LineWidth": 1,                        "IsShow": "T"                    },                    "MA_MONTH3": {                        "Name": "五年",                        "Count": 60,                        "Color": "#1919B3",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_MONTH4": {                        "Name": "十年",                        "Count": 120,                        "Color": "#8000FF",                        "LineWidth": 1,                        "IsShow": "T"                    },                    "MA_MONTH5": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_MONTH6": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_MONTH7": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_MONTH8": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_QUAR0": {                        "Name": "年",                        "Count": 4,                        "Color": "#0080FF",                        "LineWidth": 1,                        "IsShow": "T"                    },                    "MA_QUAR1": {                        "Name": "三年",                        "Count": 12,                        "Color": "#FF0080",                        "LineWidth": 1,                        "IsShow": "T"                    },                    "MA_QUAR2": {                        "Name": "五年",                        "Count": 20,                        "Color": "#1919B3",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_QUAR3": {                        "Name": "十年",                        "Count": 40,                        "Color": "#8000FF",                        "LineWidth": 1,                        "IsShow": "T"                    },                    "MA_QUAR4": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_QUAR5": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_QUAR6": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_QUAR7": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_QUAR8": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_YEAR0": {                        "Name": "三年",                        "Count": 3,                        "Color": "#FF0080",                        "LineWidth": 1,                        "IsShow": "T"                    },                    "MA_YEAR1": {                        "Name": "五年",                        "Count": 5,                        "Color": "#1919B3",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_YEAR2": {                        "Name": "十年",                        "Count": 10,                        "Color": "#8000FF",                        "LineWidth": 1,                        "IsShow": "T"                    },                    "MA_YEAR3": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_YEAR4": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_YEAR5": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_YEAR6": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_YEAR7": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "MA_YEAR8": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1,                        "IsShow": "F"                    },                    "DividendFlag": {                        "IsShow": "F"                    }                }            },            "Volume": {                "PaintItems": {                    "MA_DATE0": {                        "Name": "月均量",                        "Count": 21,                        "Color": "#FF8000",                        "LineWidth": 1.5,                        "IsShow": "T"                    },                    "MA_DATE1": {                        "Name": "季均量",                        "Count": 62,                        "Color": "#0080FF",                        "LineWidth": 1.5,                        "IsShow": "T"                    },                    "MA_DATE2": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1.5,                        "IsShow": "F"                    },                    "MA_DATE3": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1.5,                        "IsShow": "F"                    },                    "MA_WEEK0": {                        "Name": "季均量",                        "Count": 13,                        "Color": "#ff8000",                        "LineWidth": 1.5,                        "IsShow": "T"                    },                    "MA_WEEK1": {                        "Name": "年均量",                        "Count": 52,                        "Color": "#0080ff",                        "LineWidth": 1.5,                        "IsShow": "T"                    },                    "MA_WEEK2": {                        "Name": "",                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1.5,                        "IsShow": "F"                    },                    "MA_WEEK3": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1.5,                        "IsShow": "F"                    },                    "MA_MONTH0": {                        "Name": "年均量",                        "Count": 12,                        "Color": "#FF8000",                        "LineWidth": 1.5,                        "IsShow": "T"                    },                    "MA_MONTH1": {                        "Name": "三年均量",                        "Count": 36,                        "Color": "#0080FF",                        "LineWidth": 1.5,                        "IsShow": "T"                    },                    "MA_MONTH2": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1.5,                        "IsShow": "F"                    },                    "MA_MONTH3": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1.5,                        "IsShow": "F"                    },                    "MA_QUAR0": {                        "Name": "三年均量",                        "Count": 12,                        "Color": "#FF8000",                        "LineWidth": 1.5,                        "IsShow": "T"                    },                    "MA_QUAR1": {                        "Name": "十年均量",                        "Count": 40,                        "Color": "#0080FF",                        "LineWidth": 1.5,                        "IsShow": "T"                    },                    "MA_QUAR2": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1.5,                        "IsShow": "F"                    },                    "MA_QUAR3": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1.5,                        "IsShow": "F"                    },                    "MA_YEAR0": {                        "Name": "三年均量",                        "Count": 3,                        "Color": "#FF8000",                        "LineWidth": 1.5,                        "IsShow": "T"                    },                    "MA_YEAR1": {                        "Name": "十年均量",                        "Count": 10,                        "Color": "#0080FF",                        "LineWidth": 1.5,                        "IsShow": "T"                    },                    "MA_YEAR2": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1.5,                        "IsShow": "F"                    },                    "MA_YEAR3": {                        "Name": null,                        "Count": null,                        "Color": "#000000",                        "LineWidth": 1.5,                        "IsShow": "F"                    }                }            },            "RSI": {                "PaintItems": {                    "RSI1": {                        "nDays": "6"                    },                    "RSI2": {                        "nDays": "12"                    }                }            },            "KD": {                "nDays": "9",                "PaintItems": {                    "K": {                        "Color": "#ff0000",                        "IsShow": "T"                    },                    "D": {                        "Color": "#008000",                        "IsShow": "T"                    },                    "J": {                        "Color": "#8000ff",                        "IsShow": "T"                    },                    "RSV": {                        "IsShow": "T",                        "Color": "#808080"                    }                }            },            "MACD": {                "nEMA1": "12",                "nEMA2": "26",                "nDays": "9",                "nMultiOSC": "2",                "PaintItems": {                    "DIF": {                        "Color": "#ffa500",                        "IsShow": "T"                    },                    "MACD": {                        "Color": "#008000",                        "IsShow": "T"                    },                    "OSC": {                        "RiseColor": "#0033cc",                        "FallColor": "#668cff",                        "IsShow": "T"                    }                }            }        },        "CrossLineAndTipMgr": {            "ShowCrossLine": "Float",            "ShowTipMgr": "F",            "TipMgrPosition": "Hide"        }    },    "Local": {        "ChartItems": {            "Volume": {                "IsShow": "T"            },            "RSI": {                "IsShow": "T"            },            "KD": {                "IsShow": "T"            },            "MACD": {                "IsShow": "T"            },            "BIAS": {                "IsShow": "T"            }        }    }}');
    var ChartOptions = {  //ChartOptions名稱必須固定,不可變更
        Language: 'TW', SystemNM: 'StockInfo', AspNM: 'SHOWK_CHARTDATA', ChartCat: 'DATE',
        StockID: '$STOCK_ID', StockNM: '台灣50',
        BackgroundColor: 'white', Font: '11px Arial', 
        ShowStart: 100 * (StockData.RecordCount - Math.min(ChartConfig.Global.InitIndexLength, StockData.RecordCount)) / StockData.RecordCount,  //定義資料的起始顯示位置點
        ShowEnd: 100,  //定義資料的結束顯示位置點
        ChartMargin: { left: 54.5, right: 54.5 },  //全圖的左右邊距
        ChartWidth: $id(oParms.CanvasID).width - 54.5 - 54.5,  //圖寬設定值
        ChartItems: {
            KLine: {  //定義K線圖位置
                ID: 'KLine', Name: 'K線圖', IsShow: 'AlwaysShow',
                Font: '11px Arial', //定義字體
                BorderColor: 'gray', BorderLineWidth: 1, BorderLineStyle: 'solid', //定義邊框的顏色,寬度,格式
                SplitLineColor: '#eeeeee',  SplitLineWidth: 1, SplitLineStyle: 'solid', //定義底線的顏色,寬度,格式
                InitHLineCount: 10, InitVLineCount: 8,  //定義橫線及縱線數量
                Margin: { top: 36, bottom: 18 },
                Legend: { Type: 'KLine', InitHeight: 31, Font: '12px Arial', TextBaseline: 'bottom' },
                Region: { InitHeight: 148 },
                xAxis: { Font: '11px Arial', Color: 'black', Align: 'center', TextBaseline: 'top', ShowText: 'T' },
                yAxisL: { Font: '11px Arial', Color: 'black', Align: 'right', TextBaseline: 'middle', ShowSplitLine: 'T' },
                yAxisR: { Font: '11px Arial', Color: 'black', Align: 'left', TextBaseline: 'middle', ShowSplitLine: 'F' },
                PaintOrder:['MA_DATE8','MA_DATE7','MA_DATE6','MA_DATE5','MA_DATE4',
                            'MA_DATE3','MA_DATE2','MA_DATE1','MA_DATE0','KLine','DividendFlag'], //另外定義繪圖順序
                PaintItems: {
                    KLine: { 
                        Type: 'Candle', yAxisAlign: 'yAxisL', BarWidth: 5, SpaceWidth: 2, RiseColor: 'red', FallColor: 'green',
                        Fields: { Open: '開盤價', Close: '收盤價', High: '最高價', Low: '最低價' },
                        BandTag: {/*有宣告BandTag才會顯示波段數值*/} 
                    },
                    MA_DATE0: { Type: 'Line', yAxisAlign: 'yAxisL', Fields: { Field: 'MA_KLine0' } },
                    MA_DATE1: { Type: 'Line', yAxisAlign: 'yAxisL', Fields: { Field: 'MA_KLine1' } },
                    MA_DATE2: { Type: 'Line', yAxisAlign: 'yAxisL', Fields: { Field: 'MA_KLine2' } },
                    MA_DATE3: { Type: 'Line', yAxisAlign: 'yAxisL', Fields: { Field: 'MA_KLine3' } },
                    MA_DATE4: { Type: 'Line', yAxisAlign: 'yAxisL', Fields: { Field: 'MA_KLine4' } },
                    MA_DATE5: { Type: 'Line', yAxisAlign: 'yAxisL', Fields: { Field: 'MA_KLine5' } },
                    MA_DATE6: { Type: 'Line', yAxisAlign: 'yAxisL', Fields: { Field: 'MA_KLine6' } },
                    MA_DATE7: { Type: 'Line', yAxisAlign: 'yAxisL', Fields: { Field: 'MA_KLine7' } },
                    MA_DATE8: { Type: 'Line', yAxisAlign: 'yAxisL', Fields: { Field: 'MA_KLine8' } },
                    DividendFlag: { Type: 'Flag', yAxisAlign: 'yAxisL', Pointer: '▼', Color: '#000000', TextBaseline: 'bottom', 
                                    Offset: { x: 0, y: -12 }, IsShow:'F',
                                    Fields: { Field: 'DIVIDEND_FLAG', PositionRefer: '最高價' } }
                },
            },
            Volume: {  //成交量能圖位置定義
                ID: 'Volume', Name: '成交量能圖', IsShow: 'T',
                Font: '11px Arial', //定義字體
                BorderColor: 'gray', BorderLineWidth: 1, BorderLineStyle: 'solid', //定義字體與邊框的顏色,寬度,格式
                SplitLineColor: '#eeeeee',  SplitLineWidth: 1, SplitLineStyle: 'solid', //定義底線的顏色,寬度,格式
                InitHLineCount: 3.6, InitVLineCount: 8,  //定義橫線及縱線數量
                Margin: { top: 18, bottom: 0 },
                Legend: { InitHeight: 16, Font: '12px Arial', TextBaseline: 'bottom' },
                Region: { InitHeight: 45 },
                yAxisL: { Font: '11px Arial', Color: 'black', Align: 'right', TextBaseline: 'middle', BigNumberToText: 'T', InitAxisLow: 0, LimitAxisLow: 0, ShowSplitLine: 'T' },
                yAxisR: { Font: '11px Arial', Color: 'black', Align: 'left', TextBaseline: 'middle', BigNumberToText: 'T' },
                PaintOrder: ['Volume','MA_DATE2','MA_DATE1','MA_DATE0'],
                PaintItems: {
                    Volume: { Type: 'Bar', Name: '成交量', yAxisAlign: 'yAxisL', BarWidth: 5, SpaceWidth: 2, RiseColor: 'red', FallColor: 'green', 
                              Fields: { Field: '成交量', ColorField: '漲跌價' },
                              Legend: { Align: 'right', ShowTrend: 'F', Unit: '張', BigNumberToText: 'T' } },
                    MA_DATE0: { Type: 'Line', yAxisAlign: 'yAxisL', Fields: { Field: 'MA_Volume0' },
                                         Legend: { Align: 'left', ShowTrend: 'T', BigNumberToText: 'T', Unit: '張' } },
                    MA_DATE1: { Type: 'Line', yAxisAlign: 'yAxisL', Fields: { Field: 'MA_Volume1' },
                                         Legend: { Align: 'left', ShowTrend: 'T', BigNumberToText: 'T', Unit: '張' } },
                    MA_DATE2: { Type: 'Line', yAxisAlign: 'yAxisL', Fields: { Field: 'MA_Volume2' },
                                         Legend: { Align: 'left', ShowTrend: 'T', BigNumberToText: 'T', Unit: '張' } },
                },
            },
            RSI: {  //RSI定義
                ID: 'RSI', Name: 'RSI', IsShow: 'F',  //Defined in ChratConfig
                Font: '11px Arial', //定義字體
                BorderColor: 'gray', BorderLineWidth: 1, BorderLineStyle: 'solid', //定義字體與邊框的顏色,寬度,格式
                SplitLineColor: '#eeeeee',  SplitLineWidth: 1, SplitLineStyle: 'solid', //定義底線的顏色,寬度,格式
                InitVLineCount: 8,  //定義縱線數量
                Margin: { top: 16, bottom: 0 },
                Legend: { InitHeight: 16, Font: '12px Arial', TextBaseline: 'bottom', Title: { Name: '【RSI】', Align: 'right' } },
                Region: { InitHeight: 45 },
                yAxisL: { Font: '11px Arial', Color: 'black', Align: 'right', TextBaseline: 'middle', ShowSplitLine: 'T',
                          InitAxisHigh: 100, InitAxisLow: 0, LimitAxisHigh: 100, LimitAxisLow: 0, SplitPoint: [0, 20, 50, 80, 100] },
                yAxisR: { Font: '11px Arial', Color: 'black', Align: 'left', TextBaseline: 'middle' },
                PaintItems: {
                    RSI1: { nDays: null, Type: 'Line', yAxisAlign: 'yAxisL', LineWidth: 1, Color: '#FF0000', IsShow: 'T', 
                            Fields: { Field: 'RSI1' }, Legend: { Align: 'left', ShowTrend: 'T' } },
                    RSI2: { nDays: null, Type: 'Line', yAxisAlign: 'yAxisL', LineWidth: 1, Color: '#008000', IsShow: 'T', 
                            Fields: { Field: 'RSI2' }, Legend: { Align: 'left', ShowTrend: 'T' } },
                }
            },
            KD: {  //KD圖位置定義
                ID: 'KD', Name: 'KD指標', nDays: null, IsShow: 'T',  //Defined in ChratConfig
                Font: '11px Arial', //定義字體
                BorderColor: 'gray', BorderLineWidth: 1, BorderLineStyle: 'solid', //定義字體與邊框的顏色,寬度,格式
                SplitLineColor: '#eeeeee',  SplitLineWidth: 1, SplitLineStyle: 'solid', //定義底線的顏色,寬度,格式
                InitVLineCount: 8,  //定義縱線數量
                Margin: { top: 16, bottom: 0 },
                Legend: { InitHeight: 16, Font: '12px Arial', TextBaseline: 'bottom', Title: { Name: '【KD指標】', Align: 'right' } },
                Region: { InitHeight: 45 },
                yAxisL: { Font: '11px Arial', Color: 'black', Align: 'right', TextBaseline: 'middle', ShowSplitLine: 'T',
                          InitAxisHigh: 100, InitAxisLow: 0, SplitPoint: [0, 20, 50, 80, 100] },
                yAxisR: { Font: '11px Arial', Color: 'black', Align: 'left', TextBaseline: 'middle' },
                PaintOrder: ['RSV','J','D','K'],
                PaintItems: {
                    K: { Name: 'K', Type: 'Line', yAxisAlign: 'yAxisL', LineWidth: 1, Color: '#FF0000', IsShow: 'T', Fields: { Field: 'K值' },
                         Legend: { Align: 'left', ShowTrend: 'T' } },
                    D: { Name: 'D', Type: 'Line', yAxisAlign: 'yAxisL', LineWidth: 1, Color: '#008000', IsShow: 'T', Fields: { Field: 'D值' },
                         Legend: { Align: 'left', ShowTrend: 'T' } },
                    J: { Name: 'J', Type: 'Line', yAxisAlign: 'yAxisL', LineWidth: 1, Color: '#8000FF', IsShow: 'T', Fields: { Field: 'J值' },
                         Legend: { Align: 'left', ShowTrend: 'T' } },
                    RSV: { Name: 'RSV', Type: 'Line', yAxisAlign: 'yAxisL', LineWidth: 1, Color: '#808080', IsShow: 'T', Fields: { Field: 'RSV' },
                           Legend: { Align: 'left', ShowTrend: 'T' } }
                }
            },
            MACD: {  //MACD圖位置定義
                ID: 'MACD', Name: 'MACD', IsShow: 'F', 
                nEMA1: null, nEMA2: null, nDays: null, nMultiOSC: null, //Defined in ChartConfig
                Font: '11px Arial', //定義字體
                BorderColor: 'gray', BorderLineWidth: 1, BorderLineStyle: 'solid', //定義邊框的顏色,寬度,格式
                SplitLineColor: '#eeeeee',  SplitLineWidth: 1, SplitLineStyle: 'solid', //定義底線的顏色,寬度,格式
                InitHLineCount: 3.6, InitVLineCount: 8,  //定義橫線及縱線數量
                Margin: { top: 16, bottom: 0 },
                Legend: { InitHeight: 16, Font: '12px Arial', TextBaseline: 'bottom', Title: { Name: '【MACD】', Align: 'right' } },
                Region: { InitHeight: 45 },
                yAxisL: { Font: '11px Arial', Color: 'black', Align: 'right', TextBaseline: 'middle', ShowSplitLine: 'T', IsSymmetrical: 'T' },
                yAxisR: { Font: '11px Arial', Color: 'black', Align: 'left', TextBaseline: 'middle', ShowSplitLine: 'F' },
                PaintOrder: ['OSC','MACD','DIF'],
                PaintItems: {
                    DIF:  { Name: 'DIF', Type: 'Line', yAxisAlign: 'yAxisL', LineWidth: 1.2, Color: '#FFA500', IsShow: 'T', 
                            Fields: { Field: 'DIF' }, Legend: { Align: 'left', ShowTrend: 'T' } },
                    MACD: { Name: 'MACD', Type: 'Line', yAxisAlign: 'yAxisL', LineWidth: 1.2, Color: '#008000', IsShow: 'T', 
                            Fields: { Field: 'MACD' }, Legend: { Align: 'left', ShowTrend: 'T' } },
                    OSC:  { Name: 'OSC', Type: 'Bar', yAxisAlign: 'yAxisL', BarWidth: 5, SpaceWidth: 2, RiseColor: '#0033CC', FallColor: '#668CFF', 
                            Fields: { Field: 'OSC', ColorField: 'OSC' }, Legend: { Align: 'left', ShowTrend: 'T' } }
                },
            },
            BIAS: {  //BIAS定義
                ID: 'BIAS', Name: '均線乖離率', IsShow: 'F',
                Font: '11px Arial', //定義字體
                BorderColor: 'gray', BorderLineWidth: 1, BorderLineStyle: 'solid', //定義字體與邊框的顏色,寬度,格式
                SplitLineColor: '#eeeeee',  SplitLineWidth: 1, SplitLineStyle: 'solid', //定義底線的顏色,寬度,格式
                InitHLineCount: 3.6, InitVLineCount: 8,  //定義縱線數量
                Margin: { top: 16, bottom: 0 },
                Legend: { InitHeight: 16, Font: '12px Arial', TextBaseline: 'bottom', Title: { Name: '【乖離率】', Align: 'right' } },
                Region: { InitHeight: 45 },
                yAxisL: { Font: '11px Arial', Color: 'black', Align: 'right', TextBaseline: 'middle', ShowSplitLine: 'T' },
                yAxisR: { Font: '11px Arial', Color: 'black', Align: 'left', TextBaseline: 'middle' },
                PaintOrder: 'desc',
                PaintItems: {
                    BIAS0: { ReferItem: 'MA_DATE0', Type: 'Line', yAxisAlign: 'yAxisL', LineWidth: 1, Fields: { Field: 'BIAS0' }, Legend: { Align: 'left', ShowTrend: 'T', ShowPlusFlag: 'T', Unit: '%' } },
                    BIAS1: { ReferItem: 'MA_DATE1', Type: 'Line', yAxisAlign: 'yAxisL', LineWidth: 1, Fields: { Field: 'BIAS1' }, Legend: { Align: 'left', ShowTrend: 'T', ShowPlusFlag: 'T', Unit: '%' } },
                    BIAS2: { ReferItem: 'MA_DATE2', Type: 'Line', yAxisAlign: 'yAxisL', LineWidth: 1, Fields: { Field: 'BIAS2' }, Legend: { Align: 'left', ShowTrend: 'T', ShowPlusFlag: 'T', Unit: '%' } },
                    BIAS3: { ReferItem: 'MA_DATE3', Type: 'Line', yAxisAlign: 'yAxisL', LineWidth: 1, Fields: { Field: 'BIAS3' }, Legend: { Align: 'left', ShowTrend: 'T', ShowPlusFlag: 'T', Unit: '%' } },
                    BIAS4: { ReferItem: 'MA_DATE4', Type: 'Line', yAxisAlign: 'yAxisL', LineWidth: 1, Fields: { Field: 'BIAS4' }, Legend: { Align: 'left', ShowTrend: 'T', ShowPlusFlag: 'T', Unit: '%' } },
                    BIAS5: { ReferItem: 'MA_DATE5', Type: 'Line', yAxisAlign: 'yAxisL', LineWidth: 1, Fields: { Field: 'BIAS5' }, Legend: { Align: 'left', ShowTrend: 'T', ShowPlusFlag: 'T', Unit: '%' } },
                    BIAS6: { ReferItem: 'MA_DATE6', Type: 'Line', yAxisAlign: 'yAxisL', LineWidth: 1, Fields: { Field: 'BIAS6' }, Legend: { Align: 'left', ShowTrend: 'T', ShowPlusFlag: 'T', Unit: '%' } },
                    BIAS7: { ReferItem: 'MA_DATE7', Type: 'Line', yAxisAlign: 'yAxisL', LineWidth: 1, Fields: { Field: 'BIAS7' }, Legend: { Align: 'left', ShowTrend: 'T', ShowPlusFlag: 'T', Unit: '%' } },
                    BIAS8: { ReferItem: 'MA_DATE8', Type: 'Line', yAxisAlign: 'yAxisL', LineWidth: 1, Fields: { Field: 'BIAS8' }, Legend: { Align: 'left', ShowTrend: 'T', ShowPlusFlag: 'T', Unit: '%' } },
                }
            },
            ControllerBox: {  //控制桿背景圖位置定義 (物件名稱不可變更)
                BorderColor: 'gray', BorderLineWidth: 1, BorderLineStyle: 'solid', //定義邊框的顏色,寬度,格式
                SplitLineColor: '#eeeeee',  SplitLineWidth: 1, SplitLineStyle: 'solid', //定義底線的顏色,寬度,格式
                HLineCount: 1, VLineCount: 0,  //定義橫線及縱線數量
                FillColor: 'lightgray', Alpha: 0.5,  //線圖底色及透明度
                Margin: { top: 8, bottom: 0 },
                Region:{ InitHeight: 50 },
                // 控制桿大小定義
                Controller:{ MinBarDistance: 0.01, Bar: { width: 30, height: 45, BorderColor: 'black', FillColor: 'lightgray' } }
            }
        },
        CrossLineAndTipMgr: {  //十字線與資訊框定義 (物件名稱不可變更)
            GetCrossPoint: function (ev) {
                ResetIndexCurrent(ev, StockPainter);  //取得目前的Index位置
                return { x: GetXbyIDX(StockPainter.ChartOptions.IndexCurrent, StockPainter), y: GetYbyCrossPoint(StockPainter, ev) };
            },
            TipOptions: {
                GetTipHtml: function (ev) {
                    ResetLegend(StockPainter);
                    var TipHtml = GetTipHtml(StockPainter, ev);
                    TipHtml.Tip = GetTipText(StockPainter);
                    return TipHtml;
                },
                Size: { width: 220, height: null },  //資訊框的大小
                Position: { x: false, y: 20 },  //position中的值是相對於canvas的左上角的
                Opacity: 88, //透明度
                CssClass: '',
                OffsetToPoint: 30,  //資訊框與游標的距離
                TipAxisL: { Opacity: 92, Color: 'orange', BackgroundColor: 'white' },
                TipAxisR: { Opacity: 92, Color: 'orange', BackgroundColor: 'white' }
            },
            CrossLineOptions: { Color: 'black', LineWidth: 1 }
        }
    };

	// 初始化資料
	InitData(StockData, ChartOptions, ChartConfig);
                if(StockData.Records.RPT_TIME.length>600){
                    StockData.Records.RPT_TIME=StockData.Records.RPT_TIME.slice(StockData.Records.RPT_TIME.length-600);
                }
                if(StockData.Records.MACD.length>600){
                    StockData.Records.MACD=StockData.Records.MACD.slice(StockData.Records.MACD.length-600);
                }
                if(StockData.Records.K值.length>600){
                    StockData.Records.K值=StockData.Records.K值.slice(StockData.Records.K值.length-600);
                }
                if(StockData.Records.D值.length>600){
                    StockData.Records.D值=StockData.Records.D值.slice(StockData.Records.D值.length-600);
                }
                if(StockData.Records.RSV.length>600){
                    StockData.Records.RSV=StockData.Records.RSV.slice(StockData.Records.RSV.length-600);
                }
                if(StockData.Records.OSC.length>600){
                    StockData.Records.OSC=StockData.Records.OSC.slice(StockData.Records.OSC.length-600);
                }
                if(StockData.Records.DIF.length>600){
                    StockData.Records.DIF=StockData.Records.DIF.slice(StockData.Records.DIF.length-600);
                }
                if(StockData.Records.成交量.length>600){
                    StockData.Records.成交量=StockData.Records.成交量.slice(StockData.Records.成交量.length-600);
                }
                if(StockData.Records.收盤價.length>600){
                    StockData.Records.收盤價=StockData.Records.收盤價.slice(StockData.Records.收盤價.length-600);
                }
                if(StockData.Records.開盤價.length>600){
                    StockData.Records.開盤價=StockData.Records.開盤價.slice(StockData.Records.開盤價.length-600);
                }
                if(StockData.Records.開盤價.length>600){
                    StockData.Records.開盤價=StockData.Records.開盤價.slice(StockData.Records.開盤價.length-600);
                }
	return StockData;
	// 開始繪圖
	/*$id(oParms.CanvasID).style.display = '';
    var oStockChart = new StockChart(oParms, ChartConfig, ChartOptions);
    StockPainter = new Painter(oStockChart, StockData);
    StockPainter.Paint();*/
}

function InitData(StockData, ChartOptions, ChartConfig) {

    ChartConfigAlign(ChartOptions, ChartConfig.Global, 'AlignValue');  //將設定值與Global標準值進行同步修正
    ChartConfigAlign(ChartOptions, ChartConfig.Local, 'AlignValue');  //將設定值與Local標準值進行同步修正

	// 計算波段高低點
    InitKLineBandData(StockData, ChartOptions, ChartOptions.ChartItems.KLine.PaintItems.KLine);

	// 計算價格移動平均線
	InitMA_Data(StockData, ChartOptions.ChartItems.KLine, 'MA_' + ChartOptions.ChartCat,
                ChartOptions.ChartItems.KLine.PaintItems.KLine.Fields.Close);
    
	// 計算量能移動平均線
	InitMA_Data(StockData, ChartOptions.ChartItems.Volume, 'MA_' + ChartOptions.ChartCat,
                ChartOptions.ChartItems.Volume.PaintItems.Volume.Fields.Field);

	// 計算RSI
    InitRSI_Data(StockData, ChartOptions.ChartItems.RSI);

    // 計算RSV,K,D,J
    InitKD_Data(StockData, ChartOptions.ChartItems.KD);

    // 計算MACD
    InitMACD_Data(StockData, ChartOptions.ChartItems.MACD);

	// 計算BIAS
	InitBIAS_Data(StockData, ChartOptions, ChartOptions.ChartItems.KLine, ChartOptions.ChartItems.BIAS);
}

JSON.stringify(_InitStockChart({CanvasID:'StockCanvas'}));