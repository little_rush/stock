<%-- 
    Document   : allstock
    Created on : 2015/10/15, 下午 03:54:45
    Author     : sofa
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>hahaha welcom to stock family</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel=stylesheet type="text/css" href="allstock.css">
        <script src="https://code.jquery.com/jquery-2.1.4.js"></script>
        <script>
            //顯示和隱藏股票的資料
            $(document).ready(function () {
                $("#food_button").click(function () {
                    $(".stockMenu").hide();
                    $("#food").show();
                });

                $("#cement_button").click(function () {
                    $(".stockMenu").hide();
                    $("#cement").show();
                });

                $("#plastic_button").click(function () {
                    $(".stockMenu").hide();
                    $("#plastic").show();
                });

                $("#spinning_button").click(function () {
                    $(".stockMenu").hide();
                    $("#spinning").show();
                });

                $("#motor_button").click(function () {
                    $(".stockMenu").hide();
                    $("#motor").show();
                });

                $("#cables_button").click(function () {
                    $(".stockMenu").hide();
                    $("#cables").show();
                });

                $("#chemistry_button").click(function () {
                    $(".stockMenu").hide();
                    $("#chemistry").show();
                });

                $("#medical_button").click(function () {
                    $(".stockMenu").hide();
                    $("#medical").show();
                });

                $("#glass_button").click(function () {
                    $(".stockMenu").hide();
                    $("#glass").show();
                });

                $("#paper_button").click(function () {
                    $(".stockMenu").hide();
                    $("#paper").show();
                });

                $("#steel_button").click(function () {
                    $(".stockMenu").hide();
                    $("#steel").show();
                });

                $("#rubber_button").click(function () {
                    $(".stockMenu").hide();
                    $("#rubber").show();
                });

                $("#car_button").click(function () {
                    $(".stockMenu").hide();
                    $("#car").show();
                });

                $("#semiconductor_button").click(function () {
                    $(".stockMenu").hide();
                    $("#semiconductor").show();
                });

                $("#computer_button").click(function () {
                    $(".stockMenu").hide();
                    $("#computer").show();
                });
                $("#photoelectric_button").click(function () {
                    $(".stockMenu").hide();
                    $("#photoelectric").show();
                });
                $("#network_button").click(function () {
                    $(".stockMenu").hide();
                    $("#network").show();
                });
                $("#components_button").click(function () {
                    $(".stockMenu").hide();
                    $("#components").show();
                });
                $("#channels_button").click(function () {
                    $(".stockMenu").hide();
                    $("#channels").show();
                });
                $("#information_button").click(function () {
                    $(".stockMenu").hide();
                    $("#information").show();
                });
                $("#other_electronic_button").click(function () {
                    $(".stockMenu").hide();
                    $("#other_electronic").show();
                });
                $("#establishing_button").click(function () {
                    $(".stockMenu").hide();
                    $("#establishing").show();
                });
                $("#shipping_button").click(function () {
                    $(".stockMenu").hide();
                    $("#shipping").show();
                });
                $("#tourism_button").click(function () {
                    $(".stockMenu").hide();
                    $("#tourism").show();
                });
                $("#finance_button").click(function () {
                    $(".stockMenu").hide();
                    $("#finance").show();
                });
                $("#trade_button").click(function () {
                    $(".stockMenu").hide();
                    $("#trade").show();
                });
                $("#oil_button").click(function () {
                    $(".stockMenu").hide();
                    $("#oil").show();
                });
                $("#other_button").click(function () {
                    $(".stockMenu").hide();
                    $("#other").show();
                });
            });


        </script>
    </head>
    <body>
        <script>
            $(document).ready(function () {
            <%
            request.setCharacterEncoding("utf-8");
            String account = (String) session.getAttribute("account");
            Map storedStock = new HashMap();//將使用者所選過的值存在HashMap
            Class.forName("com.mysql.jdbc.Driver");
            //Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "stock", "stock");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/stock", "root", "stock");
            Statement stmt = connection.createStatement();
            if (account != null) {
                ResultSet rs = stmt.executeQuery("SELECT distinct stock_id FROM userchoose WHERE name='" + account + "'");//distinct過濾重複的值
                while (rs.next()) {
                    storedStock.put(rs.getString("stock_id"), "");
                    out.println("$(\"[name='" + rs.getString("stock_id") + "']\").attr('disabled', 'disabled');");
                }
            }
            connection.close();
            %>
            });
        </script>
        <Form name="a1" Action="addUserChoose.jsp" Method="Post">
            <div class="container">
                <div class="content">
                    <%
                        out.print("<table border=0 align=right>");
                        out.print("<tr>");
                        out.print("<td>");
                        out.print("<a href='member.jsp'>註冊</a>");
                        out.print("</td>");
                        out.print("<td>");

                        if (session.getAttribute("account") == null) {

                            out.print("<a href='login.jsp'>登入</a>");

                        } else {
                            out.print(account);

                            out.print("<a href='logout.jsp'>登出</a>");
                        }
                        out.print("</td>");
                        out.print("</tr>");
                        out.print("</table>");
                    %>
                </div>
                <div class="content1">
                    <table border="0" width="900px" align="center">
                        <tr>
                            <td>
                                <!--button onclick="cement" id="cement_button">水泥</button-->
                                <input type="button"  id="cement_button" value="水泥"/>
                                <!--<a href="cement.jsp" class="myButton">水泥</a>-->
                            </td>
                            <td>
                                <input type="button"  id="food_button" value="食品"/>
                                <!--a href="food.jsp" class="myButton">食品</a-->
                            </td>
                            <td>
                                <input type="button"  id="plastic_button" value="塑膠"/>
                                <!--a href="plastic.jsp" class="myButton">塑膠</a-->
                            </td>
                            <td>
                                <input type="button"  id="spinning_button" value="紡織"/>
                                <!--a href="spinning.jsp" class="myButton">紡織</a-->
                            </td>
                            <td>
                                <input type="button"  id="motor_button" value="電機"/>
                                <!--a href="motor.jsp" class="myButton">電機</a-->
                            </td>
                            <td>
                                <input type="button"  id="cables_button" value="電器電纜"/>
                                <!--a href="cables.jsp" class="myButton">化學</a-->
                            </td>
                            <td>
                                <input type="button"  id="chemistry_button" value="化學"/>
                                <!--a href="chemistry.jsp" class="myButton">生技醫療</a-->
                            </td>
                            <td>
                                <input type="button"  id="medical_button" value="生技醫療"/>
                                <!--a href="medical.jsp" class="myButton">生技醫療</a-->
                            </td>
                            <td>
                                <input type="button"  id="glass_button" value="玻璃"/>
                                <!--a href="glass.jsp" class="myButton">玻璃</a-->
                            </td>
                            <td>
                                <input type="button"  id="paper_button" value="造紙"/>
                                <!--a href="paper.jsp" class="myButton">造紙</a-->
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="button" id="steel_button" value="鋼鐵">
                                <!--a href="steel.jsp" class="myButton">鋼鐵</a-->
                            </td>
                            <td>
                                <input type="button" id="rubber_button" value="橡膠">
                                <!--a href="rubber.jsp" class="myButton">橡膠</a-->
                            </td>
                            <td>
                                <input type="button" id="car_button" value="汽車">
                                <!--a href="car.jsp" class="myButton">汽車</a-->
                            </td>
                            <td>
                                <input type="button" id="semiconductor_button" value="半導體">
                                <!--a href="semiconductor.jsp" class="myButton">半導體</a-->
                            </td>
                            <td>
                                <input type="button" id="computer_button" value="電腦週邊">
                                <!--a href="computer.jsp" class="myButton">電腦週邊</a-->
                            </td>
                            <td>
                                <input type="button" id="photoelectric_button" value="光電">
                                <!--a href="photoelectric.jsp" class="myButton">光電</a-->
                            </td>
                            <td>
                                <input type="button" id="network_button" value="通信網路">
                                <!--a href="network.jsp" class="myButton">通信網路</a-->
                            </td>
                            <td>
                                <input type="button" id="components_button" value="電子零組件">
                                <!--a href="components.jsp" class="myButton">電子零組件</a-->
                            </td>
                            <td>
                                <input type="button" id="channels_button" value="電子通路">
                                <!--a href="channels.jsp" class="myButton">電子通路</a-->
                            </td>
                            <td>
                                <input type="button" id="information_button" value="資訊服務">
                                <!--a href="information.jsp" class="myButton">資訊服務</a-->
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="button" id="other_electronic_button" value="其它電子">
                                <!--a href="other_electronic.jsp" class="myButton">其它電子</a-->
                            </td>
                            <td>
                                <input type="button" id="establishing_button" value="營建">
                                <!--a href="establishing.jsp" class="myButton">營建</a-->
                            </td>
                            <td>
                                <input type="button" id="shipping_button" value="航運">
                                <!--a href="shipping.jsp" class="myButton">航運</a-->
                            </td>
                            <td>
                                <input type="button" id="tourism_button" value="觀光">
                                <!--a href="tourism.jsp" class="myButton">觀光</a-->
                            </td>
                            <td>
                                <input type="button" id="finance_button" value="金融">
                                <!--a href="finance.jsp" class="myButton">金融</a-->
                            </td>
                            <td>
                                <input type="button" id="trade_button" value="貿易百貨">
                                <!--a href="trade.jsp" class="myButton">貿易百貨</a-->
                            </td>
                            <td>
                                <input type="button" id="oil_button" value="油電燃氣">
                                <!--a href="oil.jsp" class="myButton">油電燃氣</a-->
                            </td>
                            <td>
                                <input type="button" id="other_button" value="其他">
                                <!--a href="other.jsp" class="myButton">其他</a-->
                            </td>
                        </tr>
                    </table>
                </div>
                <hr width="100%" size="5" color="black">
                <div class="content2 stockMenu" style="display:none" id="cement">
                    <table border="1" width="800px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="1101">1101 台泥</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1102">1102 亞泥</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1103">1103 嘉泥</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1104">1104 環泥</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1108">1108 幸福</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1109">1109 信大</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1110">1110 東泥</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="0050">0050 東泥</input>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="content2 stockMenu" style="display:none" id="food">
                    <table border="1" width="800px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="1201">1201 味全</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1203">1203 味王</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1210">1210 大成</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1213">1213 大飲</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1215">1215 卜蜂</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1216">1216 統一</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1217">1217 愛之味</input>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1218">1218 泰山</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1219">1219 福壽</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1220">1220 台榮</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1225">1225 福懋油</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1227">1227 佳格</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1229">1229 聯華</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1231">1231 聯華食</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1232">1232 大統益</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1233">1233 天仁</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1234">1234 黑松</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1235">1235 興泰</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1236">1236 宏亞</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1702">1702 南僑</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1737">1737 臺鹽</input>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="content2 stockMenu" style="display:none" id="plastic">
                    <table border="1" width="700px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="1301">1301 台塑</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1303">1303 南亞</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1304">1304 台聚</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1305">1305 華夏</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1307">1307 三芳</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1308">1308 亞聚</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1309">1309 台達化</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1310">1310 台苯</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1312">1312 國喬</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1312A">1312A 國喬特</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1313">1313 聯成</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1314">1314 中石化</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1315">1315 達新</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1319">1319 東陽</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1321">1321 大洋</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1323">1323 永裕</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1324">1324 地球</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1325">1325 恆大</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1326">1326 台化</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1337">1337 F-再生</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1339">1339 昭輝</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1340">1340 F-勝悅</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1715">1715 萬洲</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4306">4306 炎洲</input>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="content2 stockMenu" style="display:none" id="spinning">
                    <table border="1" width="800px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="1402">1402 遠東新</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1409">1409 新纖</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1410">1410 南染</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1413">1413 宏洲</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1414">1414 東和</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1416">1416 廣豐</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1417">1417 嘉裕</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1418">1418 東華</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1419">1419 新紡</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1423">1423 利華</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1432">1432 大魯閣</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1434">1434 福懋</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1439">1439 中和</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1440">1440 南紡</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1441">1441 大東</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1443">1443 立益</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1444">1444 力麗</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1445">1445 大宇</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1446">1446 宏和</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1447">1447 力鵬</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1449">1449 佳和</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1451">1451 年興</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1452">1452 宏益</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1453">1453 大將</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1454">1454 台富</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1455">1455 集盛</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1456">1456 怡華</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1457">1457 宜進</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1459">1459 聯發</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1460">1460 宏遠</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1463">1463 強盛</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1464">1464 得力</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1465">1465 偉全</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1466">1466 聚隆</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1467">1467 南緯</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1468">1468 昶和</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1469">1469 理隆</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1470">1470 大統染</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1472">1472 三洋紡</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1473">1473 台南</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1474">1474 弘裕</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1475">1475 本盟</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1476">1476 儒鴻</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1477">1477 聚陽</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4414">4414 如興</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4426">4426 利勤</input>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content2 stockMenu" style="display: none" id="motor">
                    <table border="1" width="900px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="1503">1503 士電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1504">1504 東元</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1506">1506 正道</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1507">1507 永大</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1512">1512 瑞利</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1513">1513 中興電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1514">1514 亞力</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1515">1515 力山</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1517">1517 利奇</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1519">1519 華城</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1521">1521 大億</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1522">1522 堤維西</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1524">1524 耿鼎</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1525">1525 江申</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1526">1526 日馳</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1527">1527 鑽全</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1528">1528 恩德</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1529">1529 樂士</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1530">1530 亞崴</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1531">1531 高林股</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1533">1533 車王電</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1535">1535 中宇</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1536">1536 和大</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1537">1537 廣隆</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1538">1538 正峰新</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1539">1539 巨庭</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1540">1540 喬福</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1541">1541 錩泰</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1558">1558 伸興</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1560">1560 中砂</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1568">1568 倉佑</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1583">1583 程泰</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1589">1589 F-永冠</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1590">1590 F-亞德</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1592">1592 F-英瑞</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2049">2049 上銀</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2228">2228 劍麟</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2231">2231 為升</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2236">2236 F-百達</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2371">2371 大同</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3167">3167 大量</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4526">4526 東台</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="4532">4532 瑞智</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4551">4551 智伸科</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5288">5288 F-豐祥</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6605">6605 帝寶</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8222">8222 寶一</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8374">8374 羅昇</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8996">8996 高力</input>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="content2 stockMenu" style="display: none" id="cables">
                    <table border="1" width="700px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="1603">1603 華電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1604">1604 聲寶</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1605">1605 華新</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1608">1608 華榮</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1609">1609 大亞</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1611">1611 中電</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1612">1612 宏泰</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1613">1613 台一</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1614">1614 三洋電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1615">1615 大山</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1616">1616 億泰</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1617">1617 榮星</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1618">1618 合機</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1626">1626 F-艾美</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4930">4930 燦星網</input>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                        </tr>

                    </table>
                </div>

                <div class="content2 stockMenu" style="display: none" id="chemistry">
                    <table border="1" width="800px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="1316">1316 上曜</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1704">1704 榮化</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1708">1708 東鹼</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1709">1709 和益</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1710">1710 東聯</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1711">1711 永光</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1712">1712 興農</input>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1713">1713 國化</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1714">1714 和桐</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1717">1717 長興</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1718">1718 中纖</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1721">1721 三晃</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1722">1722 台肥</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1723">1723 中碳</input>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1724">1724 台硝</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1725">1725 元禎</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1726">1726 永記</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1727">1727 中華化</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1730">1730 花仙子</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1732">1732 毛寶</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1735">1735 日勝化</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1773">1773 勝一</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4720">4720 德淵</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4722">4722 國精化</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4725">4725 信昌化</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4733">4733 上緯</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4755">4755 三福化</input>
                            </td>
                            <td>

                            </td>
                        </tr>
                    </table>
                </div>

                <div class="content2 stockMenu" style="display: none" id="medical">
                    <table border="1" width="800px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="1701">1701 中化</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1707">1707 葡萄王</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1720">1720 生達</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1729">1729 必翔</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1731">1731 美吾華</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1733">1733 五鼎</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1734">1734 杏輝</input>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="1736">1736 喬山</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1762">1762 中化生</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1783">1783 和康生</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1786">1786 科妍</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1789">1789 神隆</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3164">3164 景岳</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3705">3705 永信</input>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="4104">4104 佳醫</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4106">4106 雃博</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4108">4108 懷特</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4119">4119 旭富</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4133">4133 亞諾法</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4137">4137 F-麗豐</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4141">4141 F-龍燈</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="4142">4142 國光</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4144">4144 F-康聯</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4164">4164 承業醫</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4737">4737 華廣</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4746">4746 台耀</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6452">6452 F-康友</input>
                            </td>
                            <td>

                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content2 stockMenu" style="display: none" id="glass">
                    <table border="1" width="600px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="1802">1802 台玻</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1806">1806 冠軍</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1809">1809 中釉</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1810">1810 和成</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1817">1817 凱撒衛</input>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content2 stockMenu" style="display: none" id="paper">
                    <table border="1" width="800px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="1902">1902 台紙</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1903">1903 士紙</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1904">1904 正隆</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1905">1905 華紙</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1906">1906 寶隆</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1907">1907 永豐餘</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1909">1909 榮成</input>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="content2 stockMenu" style="display: none" id="steel">
                    <table border="1" width="850px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="1532">1532 勤美</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2002">2002 中鋼</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2002A">2002A 中鋼特</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2006">2006 東鋼</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2007">2007 燁興</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2008">2008 高興昌</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2009">2009 第一銅</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2010">2010 春源</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2012">2012 春雨</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2013">2013 中鋼構</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2014">2014 中鴻</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2015">2015 豐興</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2017">2017 官田鋼</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2020">2020 美亞</input>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2022">2022 聚亨</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2023">2023 燁輝</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2024">2024 志聯</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2025">2025 千興</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2027">2027 大成鋼</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2028">2028 威致</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2029">2029 盛餘</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2030">2030 彰源</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2031">2031 新光鋼</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2032">2032 新鋼</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2033">2033 佳大</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2034">2034 允強</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2038">2038 海光</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3004">3004 豐達科</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="5007">5007 三星</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5538">5538 F-東明</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9958">9958 世紀鋼</input>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content2 stockMenu" style="display: none" id="rubber">
                    <table border="1" width="700px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="2101">2101 南港</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2102">2102 泰豐</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2103">2103 台橡</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2104">2104 中橡</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2105">2105 正新</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2106">2106 建大</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2107">2107 厚生</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2108">2108 南帝</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2109">2109 華豐</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2114">2114 鑫永銓</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2115">2115 F-六暉</input>
                            </td>
                            <td>

                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content2 stockMenu" style="display: none" id="car">
                    <table border="1" width="800px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="1338">1338 F-廣華</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2201">2201 裕隆</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2204">2204 中華</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2206">2206 三陽</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2207">2207 和泰車</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2227">2227 裕日車</input>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content2 stockMenu" style="display: none" id="semiconductor">
                    <table border="1" width="850px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="1437">1437 勤益控</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2302">2302 麗正</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2303">2303 聯電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2311">2311 日月光</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2325">2325 矽品</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2329">2329 華泰</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2330">2330 台積電</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2337">2337 旺宏</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2338">2338 光罩</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2342">2342 茂矽</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2344">2344 華邦電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2351">2351 順德</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2363">2363 矽統</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2369">2369 菱生</input>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2379">2379 瑞昱</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2388">2388 威盛</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2401">2401 凌陽</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2408">2408 南亞科</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2434">2434 統懋</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2436">2436 偉詮電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2441">2441 超豐</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2449">2449 京元電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2451">2451 創見</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2454">2454 聯發科</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2458">2458 義隆</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2481">2481 強茂</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3006">3006 晶豪科</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3014">3014 聯陽</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="3016">3016 嘉晶</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3034">3034 聯詠</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3035">3035 智原</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3041">3041 揚智</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3054">3054 立萬利</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3094">3094 聯傑</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3189">3189 景碩</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="3257">3257 虹冠電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3413">3413 京鼎</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3443">3443 創意</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3474">3474 華亞科</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3519">3519 綠能</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3532">3532 台勝科</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3536">3536 誠創</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="3545">3545 敦泰</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3559">3559 全智科</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3579">3579 尚志</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3583">3583 辛耘</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3588">3588 通嘉</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3598">3598 奕力</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3661">3661 F-世芯</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="3686">3686 達能</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4919">4919 新唐</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4952">4952 凌通</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5269">5269 祥碩</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5285">5285 界霖</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5305">5305 敦南</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5471">5471 松翰</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="6202">6202 盛群</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6239">6239 力成</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6243">6243 迅杰</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6257">6257 矽格</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6271">6271 同欣電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6286">6286 立錡</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6415">6415 F-矽力</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="6451">6451 F-訊芯</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8016">8016 矽創</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8081">8081 致新</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8110">8110 華東</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8131">8131 福懋科</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8150">8150 南茂</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8261">8261 富鼎</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="8271">8271 宇瞻</input>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content2 stockMenu" style="display: none" id="computer">
                    <table border="1" width="850px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox" name="2301">2301 光寶科</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2305">2305 全友</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2324">2324 仁寶</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2331">2331 精英</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2352">2352 佳世達</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2353">2353 宏碁</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2356">2356 英業達</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" name="2357">2357 華碩</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2358">2358 廷鑫</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2361">2361 鴻友</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2362">2362 藍天</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2364">2364 倫飛</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2365">2365 昆盈</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2376">2376 技嘉</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" name="2377">2377 微星</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2380">2380 虹光</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2382">2382 廣達</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2387">2387 精元</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2395">2395 研華</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2397">2397 友通</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2399">2399 映泰</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" name="2405">2405 浩鑫</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2417">2417 圓剛</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2424">2424 隴華</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2425">2425 承啟</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2442">2442 新美齊</input>
                            </td>
                            <td>
                                <input type="checkbox" name="2465">2465 麗臺</input>
                            </td>
                            <td>
                                <input type="checkbox" name="3002">3002 歐格</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" name="3005">3005 神基</input>
                            </td>
                            <td>
                                <input type="checkbox" name="3013">3013 晟銘電</input>
                            </td>
                            <td>
                                <input type="checkbox" name="3017">3017 奇鋐</input>
                            </td>
                            <td>
                                <input type="checkbox" name="3022">3022 威強電</input>
                            </td>
                            <td>
                                <input type="checkbox" name="3046">3046 建碁</input>
                            </td>
                            <td>
                                <input type="checkbox" name="3057">3057 喬鼎</input>
                            </td>
                            <td>
                                <input type="checkbox" name="3060">3060 銘異</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" name="3231">3231 緯創</input>
                            </td>
                            <td>
                                <input type="checkbox" name="3416">3416 融程電</input>
                            </td>
                            <td>
                                <input type="checkbox" name="3494">3494 誠研</input>
                            </td>
                            <td>
                                <input type="checkbox" name="3515">3515 華擎</input>
                            </td>
                            <td>
                                <input type="checkbox" name="3701">3701 大眾控</input>
                            </td>
                            <td>
                                <input type="checkbox" name="3706">3706 神達</input>
                            </td>
                            <td>
                                <input type="checkbox" name="4915">4915 致伸</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" name="4916">4916 事欣科</input>
                            </td>
                            <td>
                                <input type="checkbox" name="4938">4938 和碩</input>
                            </td>
                            <td>
                                <input type="checkbox" name="5215">5215 F-科嘉</input>
                            </td>
                            <td>
                                <input type="checkbox" name="5264">5264 F-鎧勝</input>
                            </td>
                            <td>
                                <input type="checkbox" name="6117">6117 迎廣</input>
                            </td>
                            <td>
                                <input type="checkbox" name="6128">6128 上福</input>
                            </td>
                            <td>
                                <input type="checkbox" name="6166">6166 凌華</input>
                            </td>
                        </tr>	
                        <tr>	
                            <td>
                                <input type="checkbox" name="6172">6172 互億</input>
                            </td>
                            <td>
                                <input type="checkbox" name="6206">6206 飛捷</input>
                            </td>
                            <td>
                                <input type="checkbox" name="6230">6230 超眾</input>
                            </td>
                            <td>
                                <input type="checkbox" name="6235">6235 華孚</input>
                            </td>
                            <td>
                                <input type="checkbox" name="6277">6277 宏正</input>
                            </td>
                            <td>
                                <input type="checkbox" name="6414">6414 樺漢</input>
                            </td>
                            <td>
                                <input type="checkbox" name="8114">8114 振樺電</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" name="8163">8163 達方</input>
                            </td>
                            <td>
                                <input type="checkbox" name="8210">8210 勤誠</input>
                            </td>
                            <td>
                                <input type="checkbox" name="9912">9912 偉聯</input>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        </tr>
                    </table>
                </div>
                <div class="content2 stockMenu" style="display: none" id="photoelectric">
                    <table border="1" width="850px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="2323">2323 中環</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2340">2340 光磊</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2349">2349 錸德</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2374">2374 佳能</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2393">2393 億光</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2406">2406 國碩</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2409">2409 友達</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2426">2426 鼎元</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2438">2438 翔耀</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2448">2448 晶電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2466">2466 冠西電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2475">2475 華映</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2486">2486 一詮</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2489">2489 瑞軒</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2491">2491 吉祥全</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2499">2499 東貝</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3008">3008 大立光</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3019">3019 亞光</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3024">3024 憶聲</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3031">3031 佰鴻</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3038">3038 全台</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="3049">3049 和鑫</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3050">3050 鈺德</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3051">3051 力特</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3059">3059 華晶科</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3149">3149 正達</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3356">3356 奇偶</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3383">3383 新世紀</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="3406">3406 玉晶光</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3437">3437 榮創</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3454">3454 晶睿</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3481">3481 群創</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3504">3504 揚明光</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3514">3514 昱晶</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3535">3535 晶彩科</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="3557">3557 嘉威</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3561">3561 昇陽科</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3573">3573 穎台</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3576">3576 新日光</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3584">3584 介面</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3591">3591 艾笛森</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3593">3593 力銘</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="3622">3622 洋華</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3669">3669 圓展</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3673">3673 F-TPK</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3698">3698 隆達</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4934">4934 太極</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4935">4935 F-茂林</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4942">4942 嘉彰</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="4956">4956 光鋐</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4960">4960 奇美材</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4976">4976 佳凌</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5234">5234 達興</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5243">5243 F-乙盛</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5259">5259 清惠</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5484">5484 慧友</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="6116">6116 彩晶</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6120">6120 達運</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6131">6131 悠克</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6164">6164 華興</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6168">6168 宏齊</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6176">6176 瑞儀</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6209">6209 今國光</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="6225">6225 天瀚</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6226">6226 光鼎</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6278">6278 台表科</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6289">6289 華上</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6405">6405 悅城</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6431">6431 F-光麗</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6456">6456 F-GIS</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="8072">8072 陞泰</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8105">8105 凌巨</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8215">8215 明基材</input>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content2 stockMenu" style="display: none" id="network">
                    <table border="1" width="800px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="2314">2314 台揚</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2321">2321 東訊</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2332">2332 友訊</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2345">2345 智邦</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2412">2412 中華電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2419">2419 仲琦</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2439">2439 美律</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2444">2444 友旺</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2450">2450 神腦</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2455">2455 全新</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2485">2485 兆赫</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2496">2496 卓越</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2498">2498 宏達電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3025">3025 星通</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="3027">3027 盛達</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3045">3045 台灣大</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3047">3047 訊舟</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3062">3062 建漢</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3311">3311 閎暉</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3380">3380 明泰</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3419">3419 譁裕</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="3596">3596 智易</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3682">3682 亞太電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3694">3694 海華</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3704">3704 合勤控</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4904">4904 遠傳</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4906">4906 正文</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4977">4977 F-眾達</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="4984">4984 F-科納</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5388">5388 中磊</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6136">6136 富爾特</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6142">6142 友勁</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6152">6152 百一</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6216">6216 居易</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6283">6283 淳安</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="6285">6285 啟碁</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6442">6442 光聖</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8011">8011 台通</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8101">8101 華冠</input>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content2 stockMenu" style="display: none" id="components">
                    <table border="1" width="850px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="1471">1471 首利</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1582">1582 信錦</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2059">2059 川湖</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2308">2308 台達電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2313">2313 華通</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2316">2316 楠梓電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2327">2327 國巨</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2328">2328 廣宇</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2355">2355 敬鵬</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2367">2367 燿華</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2368">2368 金像電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2375">2375 智寶</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2383">2383 台光電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2385">2385 群光</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2392">2392 正崴</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2402">2402 毅嘉</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2413">2413 環科</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2415">2415 錩新</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2420">2420 新巨</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2421">2421 建準</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2428">2428 興勤</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2429">2429 銘旺科</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2431">2431 聯昌</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2437">2437 旺詮</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2440">2440 太空梭</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2443">2443 新利虹</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2456">2456 奇力新</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2457">2457 飛宏</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2460">2460 建通</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2462">2462 良得</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2467">2467 志聖</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2472">2472 立隆</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2476">2476 鉅祥</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2478">2478 大毅</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2483">2483 百容</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2484">2484 希華</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2492">2492 華新科</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2493">2493 揚博</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3003">3003 健和興</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3011">3011 今皓</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3015">3015 全漢</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3021">3021 鴻名</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="3023">3023 信邦</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3026">3026 禾伸堂</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3032">3032 偉訓</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3037">3037 欣興</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3042">3042 晶技</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3044">3044 健鼎</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3058">3058 立德</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="3090">3090 日電貿</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3229">3229 晟鈦</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3296">3296 勝德</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3308">3308 聯德</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3338">3338 泰碩</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3376">3376 新日興</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3432">3432 台端</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="3501">3501 維熹</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3533">3533 嘉澤</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3550">3550 聯穎</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3605">3605 宏致</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3607">3607 谷崧</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3645">3645 達邁</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3653">3653 健策</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="3679">3679 新至陞</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4912">4912 F-聯德</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4958">4958 F-臻鼎</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4999">4999 鑫禾</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5469">5469 瀚宇博</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6108">6108 競國</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6115">6115 鎰勝</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="6133">6133 金橋</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6141">6141 柏承</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6153">6153 嘉聯益</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6155">6155 鈞寶</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6165">6165 捷泰</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6191">6191 精成科</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6197">6197 佳必琪</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="6205">6205 詮欣</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6213">6213 聯茂</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6224">6224 聚鼎</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6251">6251 定穎</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6269">6269 台郡</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6282">6282 康舒</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6412">6412 群電</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="6422">6422 F-君耀</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6449">6449 鈺邦</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8039">8039 台虹</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8046">8046 南電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8103">8103 瀚荃</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8213">8213 志超</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8249">8249 菱光</input>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content2 stockMenu" style="display: none" id="channels">
                    <table border="1" width="800px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="2347">2347 聯強</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2414">2414 精技</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2430">2430 燦坤</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2459">2459 敦吉</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3010">3010 華立</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3028">3028 增你強</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3033">3033 威健</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="3036">3036 文曄</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3048">3048 益登</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3055">3055 蔚華科</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3209">3209 全科</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3312">3312 弘憶股</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3315">3315 宣昶</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3702">3702 大聯大</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="5434">5434 崇越</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6145">6145 勁永</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6189">6189 豐藝</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6281">6281 全國電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8070">8070 長華</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8112">8112 至上</input>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content2 stockMenu" style="display: none" id="information">
                    <table border="1" width="800px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="2427">2427 三商電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2453">2453 凌群</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2468">2468 華經</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2471">2471 資通</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2480">2480 敦陽</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3029">3029 零壹</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3130">3130 一零四</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="4994">4994 傳奇</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5203">5203 訊連</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6112">6112 聚碩</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6183">6183 關貿</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6214">6214 精誠</input>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content2 stockMenu" style="display: none" id="other_electronic">
                    <table border="1" width="850px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="2312">2312 金寶</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2317">2317 鴻海</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2354">2354 鴻準</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2359">2359 所羅門</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2360">2360 致茂</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2373">2373 震旦行</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2390">2390 云辰</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2404">2404 漢唐</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2423">2423 固緯</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2433">2433 互盛電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2461">2461 光群雷</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2464">2464 盟立</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2474">2474 可成</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2477">2477 美隆電</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2482">2482 連宇</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2488">2488 漢平</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2495">2495 普安</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2497">2497 怡利電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3018">3018 同開</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3030">3030 德律</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3043">3043 科風</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="3305">3305 昇貿</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3450">3450 聯鈞</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3518">3518 柏騰</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3617">3617 碩天</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3665">3665 F-貿聯</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5225">5225 F-東科</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6139">6139 亞翔</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="6192">6192 巨路</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6196">6196 帆宣</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6201">6201 亞弘電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6215">6215 和椿</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6409">6409 旭隼</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8021">8021 尖點</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8201">8201 無敵</input>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content2 stockMenu" style="display: none" id="establishing">
                    <table border="1" width="850px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="1436">1436 華友聯</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1438">1438 裕豐</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1442">1442 名軒</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1805">1805 寶徠</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1808">1808 潤隆</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2501">2501 國建</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2504">2504 國產</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2505">2505 國揚</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2506">2506 太設</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2509">2509 全坤建</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2511">2511 太子</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2515">2515 中工</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2516">2516 新建</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2520">2520 冠德</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2524">2524 京城</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2527">2527 宏璟</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2528">2528 皇普</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2530">2530 華建</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2534">2534 宏盛</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2535">2535 達欣工</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2536">2536 宏普</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2537">2537 聯上發</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2538">2538 基泰</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2539">2539 櫻花建</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2540">2540 愛山林</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2542">2542 興富發</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2543">2543 皇昌</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2545">2545 皇翔</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2546">2546 根基</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2547">2547 日勝生</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2548">2548 華固</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2597">2597 潤弘</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2841">2841 台開</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2923">2923 F-鼎固</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3052">3052 夆典</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="3056">3056 總太</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3266">3266 昇陽</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="3703">3703 欣陸</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5515">5515 建國</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5519">5519 隆大</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5521">5521 工信</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5522">5522 遠雄</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="5525">5525 順天</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5531">5531 鄉林</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5533">5533 皇鼎</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5534">5534 長虹</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6177">6177 達麗</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9906">9906 欣巴巴</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9946">9946 三發</input>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="content2 stockMenu" style="display: none" id="shipping">
                    <table border="1" width="800px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="2208">2208 台船</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2603">2603 長榮海</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2605">2605 新興</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2606">2606 裕民</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2607">2607 榮運</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2608">2608 大榮</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2609">2609 陽明</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2610">2610 華航</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2611">2611 志信</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2612">2612 中航</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2613">2613 中櫃</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2615">2615 萬海</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2617">2617 台航</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2618">2618 長榮航</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2634">2634 漢翔</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2637">2637 F-慧洋</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2642">2642 宅配通</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5607">5607 遠雄港</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5608">5608 四維航</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6702">6702 興航</input>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="content2 stockMenu" style="display: none" id="tourism">
                    <table border="1" width="800px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="2701">2701 萬企</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2702">2702 華園</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2704">2704 國賓</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2705">2705 六福</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2706">2706 第一店</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2707">2707 晶華</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2712">2712 遠雄來</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2722">2722 夏都</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2723">2723 F-美食</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2727">2727 王品</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2731">2731 雄獅</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5706">5706 鳳凰</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8940">8940 新天地</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9943">9943 好樂迪</input>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="content2 stockMenu" style="display: none" id="finance">
                    <table border="1" width="850px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="2801">2801 彰銀</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2809">2809 京城銀</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2812">2812 台中銀</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2816">2816 旺旺保</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2820">2820 華票</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2823">2823 中壽</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2832">2832 台產</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2833">2833 台壽保</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2833A">2833A 台壽甲</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2834">2834 臺企銀</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2836">2836 高雄銀</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2838">2838 聯邦銀</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2845">2845 遠東銀</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2847">2847 大眾銀</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2849">2849 安泰銀</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2850">2850 新產</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2851">2851 中再保</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2852">2852 第一保</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2855">2855 統一證</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2856">2856 元富證</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2867">2867 三商壽</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2880">2880 華南金</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2881">2881 富邦金</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2882">2882 國泰金</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2883">2883 開發金</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2884">2884 玉山金</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2885">2885 元大金</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2886">2886 兆豐金</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2887">2887 台新金</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2888">2888 新光金</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2889">2889 國票金</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2890">2890 永豐金</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2891">2891 中信金</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2892">2892 第一金</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5880">5880 合庫金</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="6005">6005 群益證</input>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="content2 stockMenu" style="display: none" id="trade">
                    <table border="1" width="850px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="2601">2601 益航</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2614">2614 東森</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2901">2901 欣欣</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2903">2903 遠百</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2905">2905 三商</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2906">2906 高林</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2908">2908 特力</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="2910">2910 統領</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2911">2911 麗嬰房</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2912">2912 統一超</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2913">2913 農林</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2915">2915 潤泰全</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2929">2929 F-淘帝</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5906">5906 F-台南</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="5907">5907 F-大洋</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8429">8429 F-金麗</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8443">8443 阿瘦</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8454">8454 富邦媒</input>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="content2 stockMenu" style="display: none" id="oil">
                    <table border="1" width="800px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="2616">2616 山隆</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6505">6505 台塑化</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8926">8926 台汽電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9908">9908 大台北</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9918">9918 欣天然</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9926">9926 新海</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9931">9931 欣高</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="9937">9937 全國</input>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="content2 stockMenu" style="display: none" id="other">
                    <table border="1" width="850px" align="center">
                        <tr>
                            <td>
                                <input type="checkbox"  name="1262">1262 F-綠悅</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1435">1435 中福</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="1516">1516 川飛</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2062">2062 橋椿</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2348">2348 海悅</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2514">2514 龍邦</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="2904">2904 匯僑</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="3040">3040 遠見</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="4536">4536 拓凱</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="5871">5871 F-中租</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6184">6184 大豐電</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="6504">6504 南六</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8033">8033 雷虎</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8341">8341 日友</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="8404">8404 F-百和</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8411">8411 F-福貞</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8422">8422 可寧衛</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8427">8427 F-基勝</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="8463">8463 潤泰材</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9802">9802 F-鈺齊</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9902">9902 台火</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="9904">9904 寶成</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9905">9905 大華</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9907">9907 統一實</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9910">9910 豐泰</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9911">9911 櫻花</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9914">9914 美利達</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9917">9917 中保</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="9919">9919 康那香</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9921">9921 巨大</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9924">9924 福興</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9925">9925 新保</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9927">9927 泰銘</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9928">9928 中視</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9929">9929 秋雨</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="9930">9930 中聯資</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9933">9933 中鼎</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9934">9934 成霖</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9935">9935 慶豐富</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9938">9938 百和</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9939">9939 宏全</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9940">9940 信義</input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  name="9941">9941 裕融</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9942">9942 茂順</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9944">9944 新麗</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9945">9945 潤泰新</input>
                            </td>
                            <td>
                                <input type="checkbox"  name="9955">9955 佳龍</input>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content3" >
                    <%
                        if (session.getAttribute("account") == null) {
                            out.print("請先登入!!!!");
                        } else {
                            out.print("<input type='submit' value='確認' />");
                        }
                    %>
                </div>
            </div>

        </Form>
    </body>
</html>
